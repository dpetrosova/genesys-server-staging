'use strict';

module.exports = function(grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);
  // require('grunt-newer')(grunt);
  // require('time-grunt')(grunt);
  // require('jit-grunt')(grunt);

  grunt.initConfig({
    app : {
      source1 : 'src/main/sourceapp/1',
      dist1 : 'src/main/webapp/html/1',
      source2 : 'src/main/sourceapp/2',
      dist2 : 'src/main/webapp/html/2'
    },
    clean : {
      dist : [ '.tmp', '<%= app.dist1 %>', '<%= app.dist2 %>' ]
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass : {
      options : {
        cssDir : '.tmp/styles',
        generatedImagesDir : '.tmp/images/generated',
        importPath : './bower_components',
        httpImagesPath : '/images',
        httpGeneratedImagesPath : '/images/generated',
        httpFontsPath : '/styles/fonts',
        relativeAssets : false,
        assetCacheBuster : false,
        raw : 'Sass::Script::Number.precision = 10\n'
      },
      dist1 : {
        options : {
          sassDir : '<%= app.source1 %>/styles',
          imagesDir : '<%= app.source1 %>/images',
          javascriptsDir : '<%= app.source1 %>/scripts',
          fontsDir : '<%= app.source1 %>/styles/fonts',
          cssDir : '<%= app.dist1 %>/styles',
          generatedImagesDir : '<%= app.dist1 %>/images/generated'
        }
      }
    },

    // Copies remaining files to places other tasks can use
    copy : {
      dist1 : {
        files : [ {
          expand : true,
          dot : true,
          cwd : '<%= app.source1 %>',
          dest : '<%= app.dist1 %>',
          src : [ '*.css', 'images/**/*', 'js/{,*/}*.js', '*.{ico,png,txt}', 'styles/fonts/{,*/}*.*' ]
        }, {
          expand : true,
          cwd : '.tmp/images',
          dest : '<%= app.dist1 %>/images',
          src : [ 'generated/*' ]
        }, {
          expand : true,
          cwd : 'bower_components/bootstrap-sass/assets/fonts/bootstrap/',
          src : '*',
          dest : '<%= app.dist1 %>/styles/fonts'
        }, {
          expand : true,
          cwd : 'bower_components/fontawesome/fonts/',
          src : '*',
          dest : '<%= app.dist1 %>/styles/fonts'
        }, {
          expand : true,
          cwd : 'bower_components/fontawesome/css/',
          src : '*',
          dest : '<%= app.dist1 %>/styles/'
        }, {
          expand : true,
          cwd : 'bower_components/webfont-notosans/regular/',
          src : '*',
          dest : '<%= app.dist1 %>/styles/fonts'
        }, {
          // tinyMCE
          expand : true,
          cwd : 'bower_components/tinymce/skins',
          src : [ '**' ],
          dest : '<%= app.dist1 %>/js/skins'
        }, {
          // leaflet
          expand : true,
          cwd : 'bower_components/leaflet/dist/',
          src : [ 'leaflet.css', 'images/*.png' ],
          dest : '<%= app.dist1 %>/styles'
        }, {
          // simplecolorpicker
          expand : true,
          cwd : 'bower_components/jquery-simplecolorpicker/',
          src : [ 'jquery.simplecolorpicker.css', 'jquery.simplecolorpicker-regularfont.css' ],
          dest : '<%= app.dist1 %>/styles'
        }, {
          expand : true,
          cwd : 'bower_components/jquery-ui/themes/base/',
          src : [ 'jquery-ui.css' ],
          dest : '<%= app.dist1 %>/styles'
        } ]
      },

    // styles : {
    // expand : true,
    // cwd : '<%= app.source %>/styles',
    // dest : '.tmp/styles/',
    // src : '{,*/}*.css'
    // }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint : {
      options : {
        jshintrc : '.jshintrc',
      // reporter : require('jshint-stylish')
      },
      dist1 : {
        src : [ '<%= app.source1 %>/js/{,*/}*.js' ]
      },
      dist2 : {
        src : [ '<%= app.source2 %>/js/{,*/}*.js' ]
      },
    },

    concat : {
      options : {
      // separator: ';',
      },
      dist1 : {
        src : [ 'bower_components/jquery/dist/jquery.js', 'bower_components/modernizr/modernizr.js', 'bower_components/jquery-flot/jquery.flot.js', 'bower_components/jquery-flot/jquery.flot.pie.js',
          'bower_components/jquery-flot/jquery.flot.fillbetween.js', 'bower_components/jquery-simplecolorpicker/jquery.simplecolorpicker.js', 'bower_components/tinymce/tinymce.js',
          'bower_components/tinymce/tinymce.jquery.js', 'bower_components/tinymce/themes/modern/theme.js', 'bower_components/tinymce/plugins/link/plugin.js',
          'bower_components/tinymce/plugins/autolink/plugin.js', 'bower_components/tinymce/plugins/code/plugin.js', 'bower_components/leaflet/dist/leaflet.js',
          'bower_components/leaflet-locationfilter/src/locationfilter.js', 'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js', 'bower_components/jquery-ui/jquery-ui.js',
          'bower_components/jquery-ui/ui/autocomplete.js', 'bower_components/dyn-css/lib/dyncss.js', 'bower_components/jstree/dist/jstree.min.js' ],
        dest : '<%= app.dist1 %>/js/libraries.js',
      },
      app1 : {
        src : [ '<%= app.source1 %>/js/prologue.js', '<%= app.source1 %>/js/crophub.js', '<%= app.source1 %>/js/epilogue.js' ],
        dest : '<%= app.dist1 %>/js/genesys.js'
      },
      world1 : {
        src : [ 'bower_components/highmaps-beta/highmaps.src.js', 'bower_components/highcharts/modules/data.src.js', '<%= app.source1 %>/js/world.js' ],
        dest : '<%= app.dist1 %>/js/genesyshighcharts.js'
      }
    },

    autoprefixer : {
      options : {
        browsers : [ 'last 3 versions' ]
      },
      dist1 : {
        files : [ {
          expand : true,
          cwd : '<%= app.dist1 %>/styles',
          src : '**/*.css',
          dest : '<%= app.dist1 %>/styles'
        } ]
      }
    },

    cssmin : {
      options : {
        keepSpecialComments : 0
      },
      dist1 : {
        files : {
          '<%= app.dist1 %>/styles/bootstrap.min.css' : [ '<%= app.dist1 %>/styles/bootstrap.css' ],
          '<%= app.dist1 %>/styles/other.min.css' : [ '<%= app.dist1 %>/styles/jquery-ui.css', '<%= app.dist1 %>/styles/forza.css', '<%= app.dist1 %>/styles/leaflet.css',
            '<%= app.dist1 %>/styles/jquery.simplecolorpicker.css', '<%= app.dist1 %>/styles/jquery.simplecolorpicker-regularfont.css' ],
          '<%= app.dist1 %>/styles/genesys.min.css' : [ '<%= app.dist1 %>/styles/genesys.css' ],
          '<%= app.dist1 %>/styles/all.min.css' : [ '<%= app.dist1 %>/styles/bootstrap.css', '<%= app.dist1 %>/styles/jquery-ui.css', '<%= app.dist1 %>/styles/forza.css',
            '<%= app.dist1 %>/styles/leaflet.css', '<%= app.dist1 %>/styles/jquery.simplecolorpicker.css', '<%= app.dist1 %>/styles/jquery.simplecolorpicker-regularfont.css',
            '<%= app.dist1 %>/styles/genesys.css' ],
        }
      }
    },

    // JS min
    uglify : {
      dist1 : {
        options : {
          compress : false,
          preserveComments : false,
          report : 'min'
        },
        files : {
          '<%= app.dist1 %>/js/libraries.min.js' : [ '<%= app.dist1 %>/js/libraries.js' ],
          '<%= app.dist1 %>/js/genesys.min.js' : [ '<%= app.dist1 %>/js/genesys.js' ],
          '<%= app.dist1 %>/js/all.min.js' : [ '<%= app.dist1 %>/js/libraries.js', '<%= app.dist1 %>/js/genesys.js' ],
          '<%= app.dist1 %>/js/genesyshighcharts.min.js' : [ '<%= app.dist1 %>/js/genesyshighcharts.js' ]
        }
      }
    },

    connect : {
      server : {
        options : {
          port : 9000,
          base : 'src/main/webapp',
          livereload : true
        }
      }
    },

    watch : {
      compassdist1 : {
        files : [ 'src/main/sourceapp/1/{,*/}*.scss' ],
        tasks : [ 'compass:dist1', 'cssmin:dist1' ],
        options : {
          livereload : true
        }
      },
      js1 : {
        files : [ 'src/main/sourceapp/1/{,*/}*.js' ],
        tasks : [ 'newer:jshint:dist1', 'concat:world1', 'concat:app1' ],
        options : {
          livereload : true
        }
      }
    }

  });

  // grunt.registerTask('serve', [ 'clean', 'copy:css', 'compass:server',
  // 'autoprefixer', 'uglify:server' ]);
  // grunt.loadNpmTasks('grunt-contrib-sass');
  // grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-newer');

  grunt.registerTask('build1', [ 'newer:jshint:dist1', 'compass:dist1', 'copy:dist1', 'concat:dist1', 'concat:world1', 'concat:app1', 'autoprefixer:dist1', 'uglify:dist1', 'cssmin:dist1' ]);
  // grunt.registerTask('js', [ 'jshint:all', 'copy:dist', 'concat',
  // 'autoprefixer', 'uglify:dist' ]);
  // grunt.registerTask('css', [ 'compass:dist', 'copy:dist', 'concat',
  // 'autoprefixer', 'cssmin' ]);
  grunt.registerTask('build', [ 'build1' ]);
  grunt.registerTask('default', [ 'build' ]);

  // Start web server
  grunt.registerTask('serve', [ 'build', 'connect:server', 'watch' ]);
};
