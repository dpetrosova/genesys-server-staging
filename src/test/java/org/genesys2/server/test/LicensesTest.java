/*
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.server.test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class LicensesTest {

	@Test
	public void test3rdPartyRead() throws IOException {
		try (InputStream is = getClass().getClassLoader().getResourceAsStream("THIRD-PARTY.txt")) {
			String thirdParty = IOUtils.toString(is, Charset.forName("UTF8"));
			assertThat("THIRD-PARTY.txt could not be read", thirdParty, not(nullValue()));
			
			Pattern licenseInfo = Pattern.compile("^ {5}\\(((?:\\([^\\)]+\\)|[^\\)])+)\\) (\\(.+\\) )*(.+) \\((.+):(.+):(.+) \\- (.+)\\)$", Pattern.MULTILINE);
			Matcher m = licenseInfo.matcher(thirdParty);
			while (m.find()) {
				String license = m.group(1);
				String license2 = m.group(2);
				String project = m.group(3);
				String groupId = m.group(4);
				String artifactId = m.group(5);
				String version = m.group(6);
				String url = m.group(7);
				System.err.println(project + " url=" + url + " license=" + license + " art=" + artifactId + "@" + version);
			}
		}
	}

}
