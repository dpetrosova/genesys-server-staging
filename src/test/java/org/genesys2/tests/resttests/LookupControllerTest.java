package org.genesys2.tests.resttests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class LookupControllerTest extends AbstractRestTest {

    private static final Log LOG = LogFactory.getLog(LookupControllerTest.class);

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    User user;

    @Before
    public void startUp() throws UserException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        Country newCountry = new Country();
        newCountry.setCode2("AF");
        newCountry.setCode3("AFG");
        newCountry.setCodeNum("004");
        newCountry.setCurrent(true);
        newCountry.setName("Afghanistan");
        countryRepository.save(newCountry);
    }

    @After
    public void tearDown(){
        countryRepository.deleteAll();
    }

    @Test
    public void getTest() throws Exception {
        LOG.info("Start test-method getTest");

        ObjectMapper objectmapper = new ObjectMapper();

        Map<String, String> orgCty = new HashMap<>();
        orgCty.put("AFG", "Afghanistan");

        mockMvc.perform(get("/api/v0/lookup/orgCty")
                .contentType(MediaType.APPLICATION_JSON).param("lang", ""))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(objectmapper.writeValueAsString(orgCty)));

        LOG.info("Test getTest is passed");
    }
}
