/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.genesys.Taxonomy2;

@Entity
@Table(name = "croptaxonomy")
public class CropTaxonomy extends BusinessModel {
	private static final long serialVersionUID = 8973799991421179782L;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "cropId")
	private Crop crop;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "taxonomyId")
	private Taxonomy2 taxonomy;

	public Crop getCrop() {
		return crop;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}

	public Taxonomy2 getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(Taxonomy2 taxonomy) {
		this.taxonomy = taxonomy;
	}

}
