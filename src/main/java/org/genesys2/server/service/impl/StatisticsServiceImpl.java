/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.genesys.PDCIStatistics;
import org.genesys2.server.model.genesys.PhenoStatistics;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class StatisticsServiceImpl implements StatisticsService {
	public static final Log LOG = LogFactory.getLog(StatisticsServiceImpl.class);

	@Autowired
	private GeoService geoService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Override
	@Cacheable(value = "statistics", key = "'stats.' + #root.methodName")
	public long numberOfCountries() {
		return geoService.countActive();
	}

	@Override
	@Cacheable(value = "statistics", key = "'stats.' + #root.methodName")
	public long numberOfInstitutes() {
		return instituteService.countActive();
	}

	@Override
	@Cacheable(value = "statistics", key = "'stats.' + #root.methodName")
	public long numberOfAccessions() {
		return genesysService.countAll();
	}
	
	@Override
	@Cacheable(value = "statistics", key = "'stats.' + #root.methodName")
	public long numberOfActiveAccessions() {
		return genesysService.countAllActive();
	}

	@Override
	@Cacheable(value = "statistics", key = "'stats.' + #root.methodName")
	public long numberOfHistoricAccessions() {
		return genesysService.countAllHistoric();
	}

	@Override
	@Cacheable(unless = "#result == null", value = "statistics", key = "'stats.' + #root.methodName + '-' + #faoInstitute.id")
	public PDCIStatistics statisticsPDCI(FaoInstitute faoInstitute) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Regenerating PDCI statistics for " + faoInstitute);
		}
		return genesysService.statisticsPDCI(faoInstitute);
	}

	@Override
	@Cacheable(unless = "#result == null", value = "statistics", key = "'stats.' + #root.methodName + '-org' + #organization.id")
	public PDCIStatistics statisticsPDCI(Organization organization) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Regenerating PDCI statistics for " + organization);
		}
		return genesysService.statisticsPDCI(organization);
	}

	@Override
	@Cacheable(unless = "#result == null", value = "statistics", key = "'stats.' + #root.methodName + '-' + #faoInstitute.id")
	public PhenoStatistics statisticsPheno(FaoInstitute faoInstitute) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Regenerating C&E statistics for " + faoInstitute);
		}
		return genesysService.statisticsPheno(faoInstitute);
	}

}
