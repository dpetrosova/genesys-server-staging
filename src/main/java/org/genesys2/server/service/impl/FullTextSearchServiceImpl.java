/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.server.service.impl;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.functionScoreQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders.fieldValueFactorFunction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.genesys2.server.model.BusinessModel;
import org.genesys2.server.model.elastic.FullTextDocument;
import org.genesys2.server.model.impl.ActivityPost;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.ClassPK;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.ActivityPostRepository;
import org.genesys2.server.persistence.domain.ArticleRepository;
import org.genesys2.server.persistence.domain.CountryRepository;
import org.genesys2.server.persistence.domain.FaoInstituteRepository;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.ElasticSearchManagementService;
import org.genesys2.server.service.FullTextSearchService;
import org.genesys2.server.service.IndexAliasConstants;
import org.genesys2.server.servlet.controller.JspHelper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FullTextSearchServiceImpl implements FullTextSearchService, InitializingBean {

	private static final Log LOG = LogFactory.getLog(FullTextSearchServiceImpl.class);

	private static final String ACCESSION_SECTION = "accession";
	private static final String CLASSPK_SHORTNAME = "classPK.shortName";

	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;

	@Autowired
	private ElasticSearchManagementService elasticSearchManagementService;

	@Autowired
	private ArticleRepository articleRepository;

	@Autowired
	private ActivityPostRepository postRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private FaoInstituteRepository instituteRepository;

	@Autowired
	private JspHelper jspHelper;

	@Autowired
	private ContentService contentService;

	private final Map<String, Class<?>> clazzMap;

	public FullTextSearchServiceImpl() {
		this.clazzMap = new HashMap<String, Class<?>>();
		this.clazzMap.put(Article.class.getName(), FullTextDocument.class);
		this.clazzMap.put(ActivityPost.class.getName(), FullTextDocument.class);
		this.clazzMap.put(Country.class.getName(), FullTextDocument.class);
		this.clazzMap.put(FaoInstitute.class.getName(), FullTextDocument.class);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		LOG.info("Initializing index");
		this.elasticsearchTemplate.createIndex(FullTextDocument.class);
		LOG.info("Putting mapping");
		try {
			this.elasticsearchTemplate.putMapping(FullTextDocument.class);
		} catch (final Throwable e) {
			LOG.error("Mapping mismatch. Need to reindex.", e);
		}
		LOG.info("Refreshing");
		this.elasticsearchTemplate.refresh(FullTextDocument.class, true);

		String newIndex = IndexAliasConstants.INDEX_FULLTEXT + System.currentTimeMillis();
		if (!elasticSearchManagementService.aliasExists(IndexAliasConstants.INDEXALIAS_FULLTEXT_READ)) {
			elasticsearchTemplate.createIndex(newIndex);
			elasticsearchTemplate.putMapping(newIndex, IndexAliasConstants.PASSPORT_TYPE,
					elasticsearchTemplate.getMapping(FullTextDocument.class));
			elasticSearchManagementService.realias(IndexAliasConstants.INDEXALIAS_FULLTEXT_READ, newIndex);
		}
		if (!elasticSearchManagementService.aliasExists(IndexAliasConstants.INDEXALIAS_FULLTEXT_WRITE)) {
			if (!elasticsearchTemplate.indexExists(newIndex)) {
				elasticsearchTemplate.createIndex(newIndex);
				elasticsearchTemplate.putMapping(newIndex, IndexAliasConstants.PASSPORT_TYPE,
						elasticsearchTemplate.getMapping(FullTextDocument.class));
			}
			elasticSearchManagementService.realias(IndexAliasConstants.INDEXALIAS_FULLTEXT_WRITE, newIndex);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> search(final String query, final Pageable pageable, final Class<?> clazz) throws SearchException {
		ClassPK classPK = contentService.getClassPk(clazz);
		if (classPK == null) {
			throw new SearchException("No such fulltext type");
		}

		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withIndices(IndexAliasConstants.INDEXALIAS_FULLTEXT_READ)
				.withQuery(
						functionScoreQuery(
								boolQuery().must(matchQuery(CLASSPK_SHORTNAME, classPK.getShortName())).must(
										queryStringQuery(query).defaultOperator(QueryStringQueryBuilder.Operator.AND))).add(
								fieldValueFactorFunction("score").factor(1.0f))).withPageable(pageable).build();

		try {
			FacetedPage<FullTextDocument> fulltextResults = this.elasticsearchTemplate.queryForPage(searchQuery, FullTextDocument.class);
			return toEntities(fulltextResults, pageable);
		} catch (final Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	private Page<?> toEntities(FacetedPage<FullTextDocument> fulltextResults, Pageable pageable) {
		List<Object> content = new ArrayList<>(fulltextResults.getSize());
		for (FullTextDocument document : fulltextResults.getContent()) {
			content.add(toEntity(document.getClassPK().getClassName(), document.getId()));
		}
		return new PageImpl<>(content, pageable, fulltextResults.getTotalElements());
	}

	private Object toEntity(String className, Long id) {
		if (FaoInstitute.class.getName().equals(className)) {
			return instituteRepository.findOne(id);
		} else if (Country.class.getName().equals(className)) {
			return countryRepository.findOne(id);
		} else if (ActivityPost.class.getName().equals(className)) {
			return postRepository.findOne(id);
		} else if (Article.class.getName().equals(className)) {
			return articleRepository.findOne(id);
		} else {
			throw new UnsupportedOperationException("Missing toEntity implementation for class=" + className);
		}
	}

	@Override
	public Page<?> search(final String query, final Pageable pageable, final String section) throws SearchException {
		ClassPK classPK = contentService.getClassPk(section);
		if (classPK == null) {
			throw new SearchException("No such fulltext type");
		}

		SearchQuery searchQuery;
		Class<?> clazz;

		if (section.equals(ACCESSION_SECTION)) {
			throw new UnsupportedOperationException("Use ElasticsearchServiceImpl for accession search");
		} else {
			searchQuery = new NativeSearchQueryBuilder()
					.withIndices(IndexAliasConstants.INDEXALIAS_FULLTEXT_READ)
					.withQuery(
							functionScoreQuery(
									boolQuery().must(matchQuery(CLASSPK_SHORTNAME, section)).must(
											queryStringQuery(query).defaultOperator(QueryStringQueryBuilder.Operator.AND))).add(
									fieldValueFactorFunction("score").factor(1.0f))).withPageable(pageable).build();
			clazz = FullTextDocument.class;
		}

		try {
			return this.elasticsearchTemplate.queryForPage(searchQuery, clazz);
		} catch (final Throwable e) {
			throw new SearchException(e.getMessage(), e);
		}
	}

	@Override
	public List<FullTextDocument> getFullTextDocuments(final String className, final Collection<Long> ids) {
		final List<BusinessModel> models = new ArrayList<>(ids.size());

		// TODO Only include global articles, exclude "blurbs"
		models.addAll(this.articleRepository.findAll(ids));

		// Posts are only in "en"
		models.addAll(this.postRepository.findAll(ids));
		models.addAll(this.countryRepository.findAll(ids));
		models.addAll(this.instituteRepository.findAll(ids));

		return getFullTextDocuments(models);
	}

	@Override
	public void remove(final String className, final long id) {
		final Class<?> clazz2 = this.clazzMap.get(className);
		if (clazz2 == null) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Not removing unmapped entity from ES class=" + className + " id=" + id);
			}
			return;
		}
		if (LOG.isDebugEnabled())
			LOG.debug("Removing from index " + clazz2 + " id=" + id);

		this.elasticsearchTemplate.delete(clazz2, String.valueOf(id));
	}

	@Override
	public void updateAll(final String className, final Collection<Long> ids) {
		if (!this.clazzMap.containsKey(className)) {
			return;
		}

		if (ids.isEmpty()) {
			LOG.info("Skipping empty updateAll.");
			return;
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("Updating " + className + " bulk_size=" + ids.size());
		}

		final List<IndexQuery> queries = new ArrayList<IndexQuery>();

		final List<FullTextDocument> documents = getFullTextDocuments(className, ids);

		for (final FullTextDocument document : documents) {
			if (document == null) {
				continue; // Skip null id
			}

			final IndexQuery iq = new IndexQuery();
			iq.setIndexName(IndexAliasConstants.INDEXALIAS_FULLTEXT_WRITE);
			iq.setId(String.valueOf(document.getId()));
			iq.setObject(document);

			queries.add(iq);
		}

		if (!queries.isEmpty()) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Indexing " + className + " count=" + queries.size() + " of provided objects count=" + ids.size());
			}
			this.elasticsearchTemplate.bulkIndex(queries);
		}
	}

	private List<FullTextDocument> getFullTextDocuments(final List<BusinessModel> models) {
		final List<FullTextDocument> documents = new ArrayList<>(models.size());

		for (final BusinessModel model : models) {
			documents.add(getFullTextDocument(model));
		}

		return documents;
	}

	/**
	 * TODO FIXME
	 * 
	 * @param model
	 * @return
	 */
	private FullTextDocument getFullTextDocument(final BusinessModel model) {
		final FullTextDocument document = new FullTextDocument();

		document.setId(model.getId());
		document.setClassPK(contentService.ensureClassPK(model.getClass()));

		if (model instanceof Article) {
			// FIXME Should not include articles that belong to institutes or
			// countries
			final Article article = (Article) model;
			document.setBody(article.getBody());
			document.setSummary(article.getSummary());
			document.setLanguage(article.getLang());
			document.setCreatedDate(article.getCreatedDate());
			document.setLastModifiedDate(article.getLastModifiedDate());
			document.setUrlToContent("/content/" + article.getSlug());

		} else if (model instanceof ActivityPost) {
			updateDocument((ActivityPost) model, document);

		} else if (model instanceof Country) {
			updateDocument((Country) model, document);

		} else if (model instanceof FaoInstitute) {
			updateDocument((FaoInstitute) model, document);
		}

		return document;
	}

	private void updateDocument(ActivityPost activityPost, FullTextDocument document) {
		document.setBody(activityPost.getBody());
		document.setSummary(activityPost.getTitle());
		document.setCreatedDate(activityPost.getCreatedDate());
		document.setLastModifiedDate(activityPost.getLastModifiedDate());
		document.setUrlToContent("/content/news/" + activityPost.getId() + "/" + this.jspHelper.suggestUrlForText(activityPost.getTitle()));
	}

	private void updateDocument(Country country, FullTextDocument document) {
		document.setUrlToContent("/geo/" + country.getCode3());
		document.setLanguage(contentService.getDefaultLocale().getLanguage());
		document.setSummary(country.getCode3() + " " + country.getNameL());

		Article blurb = contentService.getArticle(country, "blurp", contentService.getDefaultLocale());
		if (blurb != null) {
			document.setBody(blurb.getSummary() + "\n\n" + blurb.getBody());
		}
	}

	private void updateDocument(FaoInstitute institute, FullTextDocument document) {
		document.setUrlToContent("/wiews/" + institute.getCode());
		document.setLanguage(contentService.getDefaultLocale().getLanguage());
		document.setSummary(institute.getCode() + " " + institute.getAcronym() + " " + institute.getFullName());

		Article blurb = contentService.getArticle(institute, "blurp", contentService.getDefaultLocale());
		if (blurb != null) {
			document.setBody(blurb.getSummary() + "\n\n" + blurb.getBody());
		}
		// If the institute has accessions, we boost it
		if (institute.getAccessionCount() > 0) {
			document.setScore(0.0f + institute.getAccessionCount());
		}
	}
}
