/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.webapi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.SearchException;
import org.genesys2.server.servlet.controller.rest.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = { "/webapi" })
public class WebApiController extends RestController {

	private final ObjectMapper mapper = new ObjectMapper();

	@Value("${base.url}")
	private String baseUrl;

	@Autowired
	GenesysService genesysService;

	@Autowired
	private ElasticService elasticService;

	/**
	 * To be removed. Use the scripts /html/js/webapi.js or
	 * /html/js/webapi.min.js
	 * 
	 * @param model
	 * @param clientId
	 * @param clientSecret
	 * @return
	 * @deprecated To be removed in May 2015
	 */
	// TODO Remove
	@Deprecated
	@RequestMapping(value = "/genesys-webapi.js", method = RequestMethod.GET)
	public String getScript(ModelMap model, @RequestParam("client_id") String clientId,
			@RequestParam(value = "client_secret", required = false) String clientSecret) {
		model.put("baseUrl", baseUrl);
		model.put("clientId", clientId);
		model.put("clientSecret", clientSecret);
		return "/webapi/genesys-webapi";
	}

	@RequestMapping(value = "/v0/acn/filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Page<AccessionDetails> filterAccessions(@RequestBody JsonData jsonData) throws IOException, SearchException {

		AppliedFilters appliedFilters = mapper.readValue(jsonData.filter, AppliedFilters.class);

		LOG.info("filteringAccessions: " + appliedFilters.toString());

		Pageable pageable = new PageRequest(jsonData.startAt - 1, Math.min(50, jsonData.maxRecords), new Sort("acceNumb"));
		Page<AccessionDetails> accessions = elasticService.filter(appliedFilters, pageable);
		return accessions;
	}

	@RequestMapping(value = "/v0/acn/overview", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, TermResult> overview(@RequestBody String filters) throws IOException, SearchException {

		AppliedFilters appliedFilters = mapper.readValue(filters, AppliedFilters.class);
		Map<String, TermResult> overview=new HashMap<String, TermResult>();
		
		overview.put("statsInstCode", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTCODE, 20));
		overview.put("statsInstCountry", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTITUTE_COUNTRY_ISO3, 20));

		overview.put("statsOrgCty", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.ORGCTY_ISO3, 20));
		overview.put("statsDonorCode", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.DONORCODE, 20));

		overview.put("statsMLS", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.MLSSTATUS, 2));
		overview.put("statsAvailable", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.AVAILABLE, 2));

		overview.put("statsHistoric", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.HISTORIC, 2));

		overview.put("statsStorage", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.STORAGE, 30));
		overview.put("statsDuplSite", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.DUPLSITE, 20));
		overview.put("statsSGSV", elasticService.termStatistics(appliedFilters, FilterConstants.SGSV, 2));

		overview.put("statsGenus", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.TAXONOMY_GENUS, 20));
		overview.put("statsSpecies", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.TAXONOMY_GENUSSPECIES, 20));
		overview.put("statsCrops", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.CROPS, 30));
		overview.put("statsSampStat", elasticService.termStatisticsAuto(appliedFilters, FilterConstants.SAMPSTAT, 30));
		
		return overview;
	}

	public static class JsonData {
		public String filter = "";
		public Integer startAt = 1;
		public Integer maxRecords = 50;
	}
}
