/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest.serialization;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.genesys.MaterialSubRequest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MaterialSubRequestSerializer extends JsonSerializer<MaterialSubRequest> {

	@Override
	public void serialize(MaterialSubRequest request, JsonGenerator jgen, SerializerProvider sp) throws IOException, JsonProcessingException {
		if (request == null) {
			jgen.writeNull();
		} else {
			jgen.writeStartObject();
			jgen.writeObjectField("uuid", request.getUuid());
			jgen.writeObjectField("version", request.getVersion());
			jgen.writeObjectField("instCode", request.getInstCode());
			jgen.writeObjectField("instEmail", request.getInstEmail());
			jgen.writeObjectField("state", request.getState());
			jgen.writeObjectField("createdDate", request.getCreatedDate());
			jgen.writeObjectField("lastModifiedDate", request.getLastModifiedDate());
			jgen.writeObjectField("lastReminderDate", request.getLastReminderDate());

			if (StringUtils.isNotBlank(request.getBody())) {
				jgen.writeRaw(",\"body\":");
				jgen.writeRaw(request.getBody());
			}
			jgen.writeEndObject();
		}
	}

}
