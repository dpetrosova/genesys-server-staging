/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.admin;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.service.DSService;
import org.genesys2.server.service.worker.WorldClimUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/ds2")
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class DS2Controller {
	public static final Log LOG = LogFactory.getLog(DS2Controller.class);

	@Autowired
	private WorldClimUpdater worldClimUpdater;

	@Autowired
	private DSService dsService;

	@RequestMapping("/")
	public String root(Model model) {
		return "/admin/ds2/index";
	}

	@RequestMapping(value = "/worldclim/update", method = RequestMethod.POST)
	public String worldClim() throws IOException {
		worldClimUpdater.update("alt");

		for (int i = 1; i <= 12; i++) {
			worldClimUpdater.update("tmin" + i);
			worldClimUpdater.update("tmax" + i);
			worldClimUpdater.update("tmean" + i);
			worldClimUpdater.update("prec" + i);
		}

		for (int i = 1; i <= 19; i++) {
			worldClimUpdater.update("bio" + i);
		}
		return "redirect:/admin/ds2/";
	}

	@RequestMapping(value = "/worldclim/delete", method = RequestMethod.POST)
	public String worldClimDelete() throws IOException {
		DS ds = dsService.loadDatasetByUuid(WorldClimUpdater.WORLDCLIM_DATASET);
		if (ds != null) {
			LOG.info("Deleting dataset " + ds.getUuid());
			for (DSDescriptor dsd : ds.getDescriptors()) {
				dsService.deleteDescriptor(dsd);
			}
			dsService.deleteRows(ds);
			dsService.deleteDataset(ds);
			LOG.info("Done");
		}
		return "redirect:/admin/ds2/";
	}
}
