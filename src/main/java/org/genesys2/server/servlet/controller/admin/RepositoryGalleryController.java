/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.ImageGallery;
import org.genesys2.server.filerepository.service.ImageGalleryService;
import org.genesys2.server.servlet.controller.BaseController;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(RepositoryGalleryController.CONTROLLER_PATH)
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class RepositoryGalleryController extends BaseController {
	public static final String CONTROLLER_PATH = "/admin/r/g";
	public static final String JSP_PATH = "/admin/repository/gallery";
	public static final Log LOG = LogFactory.getLog(RepositoryGalleryController.class);

	@Autowired
	private ImageGalleryService imageGalleryService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String listAllFiles(ModelMap model) {
		return "redirect:" + CONTROLLER_PATH + "/1";
	}

	@RequestMapping(value = "/{page:\\d+}", method = RequestMethod.GET)
	public String listAllFiles(ModelMap model, @PathVariable("page") int page) {
		Page<ImageGallery> pagedData = imageGalleryService.listImageGalleries(new PageRequest(page - 1, 50, new Sort("path")));
		model.addAttribute("pagedData", pagedData);

		return JSP_PATH + "/index";
	}

	@RequestMapping(value = "/**", method = RequestMethod.GET)
	public String listAllFiles(ModelMap model, HttpServletRequest request) {
		String fullpath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		fullpath = fullpath.substring(CONTROLLER_PATH.length());

		ImageGallery imageGallery = imageGalleryService.loadImageGallery(fullpath);

		if (imageGallery == null) {
			throw new ResourceNotFoundException("No image gallery here!");
		}

		imageGalleryService.ensureThumbnails(imageGallery, 200, 200);
		model.addAttribute("thumbnailFormat", "200x200");
		model.addAttribute("imageGallery", imageGallery);

		return JSP_PATH + "/details";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String getEditPage(@RequestParam String galleryPath, ModelMap model) throws NoSuchRepositoryFileException {
		ImageGallery imageGallery = imageGalleryService.loadImageGallery(galleryPath);
		if (imageGallery == null) {
			imageGallery = new ImageGallery();
			imageGallery.setPath(galleryPath);
		}
		model.addAttribute("imageGallery", imageGallery);

		return JSP_PATH + "/edit";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateMetadata(@ModelAttribute ImageGallery imageGallery, RedirectAttributes redirectAttributes) throws NoSuchRepositoryFileException {

		ImageGallery updatedGallery = imageGalleryService.loadImageGallery(imageGallery.getPath());
		if (updatedGallery == null) {
			updatedGallery = imageGalleryService.createImageGallery(imageGallery.getPath(), imageGallery.getTitle(), imageGallery.getDescription());
		} else {
			updatedGallery = imageGalleryService.updateImageGalery(updatedGallery, imageGallery.getTitle(), imageGallery.getDescription());
		}

		redirectAttributes.addFlashAttribute("successMessage", "repository.gallery.successfully-updated");
		return "redirect:" + CONTROLLER_PATH;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteFile(@RequestParam String galleryPath, RedirectAttributes redirectAttributes) {

		ImageGallery imageGallery = imageGalleryService.loadImageGallery(galleryPath);
		imageGalleryService.removeGallery(imageGallery);

		redirectAttributes.addFlashAttribute("successMessage", "repository.gallery.removed");
		return "redirect:" + CONTROLLER_PATH;
	}
}
