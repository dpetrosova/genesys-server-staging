/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.json.UserAccessionList;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class SelectionBean implements Serializable {
	private static final long serialVersionUID = -1084615112110837714L;

	private final Set<Long> accessionIds = new HashSet<Long>();

	private UserAccessionList userAccessionList;

	public Set<Long> getAccessionIds() {
		return accessionIds;
	}

	public void add(long id) {
		accessionIds.add(id);
	}

	public void remove(long id) {
		accessionIds.remove(id);
	}

	public void clear() {
		accessionIds.clear();
	}

	public boolean containsId(long id) {
		return accessionIds.contains(id);
	}

	public boolean contains(Accession accession) {
		return accessionIds.contains(accession.getId());
	}

	public Set<Long> copy() {
		return new HashSet<>(accessionIds);
	}

	public int size() {
		return accessionIds.size();
	}

	public void setUserAccessionList(UserAccessionList userAccessionList) {
		this.userAccessionList = userAccessionList;
	}

	public UserAccessionList getUserAccessionList() {
		return userAccessionList;
	}
}
