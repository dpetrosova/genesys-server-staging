/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.genesys2.server.model.kpi.ExecutionDimension;

public class ExecutionDimensionJson {
	public Long id;
	public long dimensionId;
	public String link;
	public String field;

	public static ExecutionDimensionJson from(ExecutionDimension executionDimension) {
		if (executionDimension == null)
			return null;
		ExecutionDimensionJson ej = new ExecutionDimensionJson();
		ej.id = executionDimension.getId();
		ej.dimensionId = executionDimension.getDimension().getId();
		ej.link = executionDimension.getLink();
		ej.field = executionDimension.getField();
		return ej;
	}

	public static Collection<ExecutionDimensionJson> from(List<ExecutionDimension> executionDimension) {
		if (executionDimension == null) {
			return null;
		}
		List<ExecutionDimensionJson> list = new ArrayList<ExecutionDimensionJson>(executionDimension.size());
		for (ExecutionDimension e : executionDimension) {
			list.add(from(e));
		}
		return list;
	}
}
