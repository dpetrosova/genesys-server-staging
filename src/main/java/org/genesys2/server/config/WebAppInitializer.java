/*
 * Copyright 2016 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.genesys2.server.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionTrackingMode;

import com.hazelcast.web.SessionListener;

import org.genesys2.server.servlet.filter.LocaleURLFilter;
import org.genesys2.server.servlet.filter.NewGUIFilter;
import org.genesys2.spring.config.ApplicationConfig;
import org.genesys2.spring.config.SpringServletConfig;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

public class WebAppInitializer implements WebApplicationInitializer {

	private static final int SESSION_TIMEOUT = 60 * 60 * 6;

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(ApplicationConfig.class);

		servletContext.addListener(new ContextLoaderListener(rootContext));
		servletContext.addListener(new org.genesys2.server.config.SessionListener(SESSION_TIMEOUT));
		servletContext.addListener(new SessionListener());

		// servletContext.setInitParameter("contextConfigLocation", "classpath:spring/application-context.xml");
		servletContext.setInitParameter("defaultHtmlEscape", "true");

		servletContext.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));
		servletContext.getSessionCookieConfig().setHttpOnly(true);

		AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
		dispatcherContext.register(SpringServletConfig.class);

		registerServlets(servletContext, dispatcherContext);

		registerFilters(servletContext, dispatcherContext);
	}

	private void registerServlets(ServletContext servletContext, AnnotationConfigWebApplicationContext appContext) {
		// Dispatcher servlet configuration
		DispatcherServlet dispatcher = new DispatcherServlet(appContext);
		dispatcher.setThrowExceptionIfNoHandlerFound(true);

		ServletRegistration.Dynamic dispatcherServlet = servletContext
				.addServlet("mvc", dispatcher);

		dispatcherServlet.setLoadOnStartup(1);
		dispatcherServlet.addMapping("/");

		// Default Jetty servlet configuration
		ServletRegistration defaultServlet = servletContext.getServletRegistration("default");

		defaultServlet.setInitParameter("dirAllowed", "false");
		defaultServlet.setInitParameter("maxCacheSize", "256000000");
		defaultServlet.setInitParameter("maxCachedFileSize", "200000000");
		defaultServlet.setInitParameter("maxCachedFiles", "2048");
		defaultServlet.setInitParameter("gzip", "true");
		defaultServlet.setInitParameter("etags", "true");
		defaultServlet.setInitParameter("useFileMappedBuffer", "true");
		defaultServlet.setInitParameter("resourceCache", "resourceCache");
		defaultServlet.setInitParameter("cacheControl", "private, max-age=3600");

		defaultServlet.addMapping("/html/*", "/index.html", "/favicon.ico");
	}

	private void registerFilters(ServletContext servletContext, AnnotationConfigWebApplicationContext appContext) {
		// UrlRewrite filter configuration
		FilterRegistration.Dynamic urlRewriteFilter = servletContext
				.addFilter("urlRewriteFilter", UrlRewriteFilter.class);

		urlRewriteFilter.setInitParameter("confReloadCheckInterval", "10");
		// urlRewriteFilter.setInitParameter("logLevel", "sysout:DEBUG");

		urlRewriteFilter.addMappingForUrlPatterns(
				EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD), false, "/*");

		// Encoding filter configuration
		FilterRegistration.Dynamic encodingFilter = servletContext
				.addFilter("encodingFilter", CharacterEncodingFilter.class);

		encodingFilter.setInitParameter("encoding", "UTF-8");
		encodingFilter.setInitParameter("forceEncoding", "true");

		encodingFilter.addMappingForUrlPatterns(null, false, "/*");

		// Configuration of cross origin filter for web REST API clients
		FilterRegistration.Dynamic crossOriginFilter = servletContext
				.addFilter("cross-origin", "org.eclipse.jetty.servlets.CrossOriginFilter");

		crossOriginFilter.setInitParameter("allowedOrigins", "*");
		crossOriginFilter.setInitParameter("allowCredentials", "true");
		crossOriginFilter.setInitParameter("allowedMethods", "GET,POST,PUT,DELETE");
		crossOriginFilter.setInitParameter("allowedHeaders", "*");
		// Do not chain preflight request to application
		crossOriginFilter.setInitParameter("chainPreflight", "false");

		crossOriginFilter.addMappingForUrlPatterns(null, false, "/oauth/token");
		crossOriginFilter.addMappingForUrlPatterns(null, false, "/api");
		crossOriginFilter.addMappingForUrlPatterns(null, false, "/webapi");

		servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy("springSecurityFilterChain"))
				.addMappingForUrlPatterns(null, false, "/*");

		// Filter chain proxy configuration
/*		FilterRegistration.Dynamic filterChainProxy = servletContext
				.addFilter("org.springframework.security.filterChainProxy",
						"org.springframework.web.filter.DelegatingFilterProxy");

		filterChainProxy.addMappingForUrlPatterns(null, false, "*//*");*/

		// Locale URL filter configuration
		FilterRegistration.Dynamic localeURLFilter = servletContext
				.addFilter("localeURLFilter", LocaleURLFilter.class);

		localeURLFilter.setInitParameter("exclude-paths", "/html /login-attempt");
		localeURLFilter.setInitParameter("default-locale", "en");
		localeURLFilter.setInitParameter("allowed-locales", "en es de fr fa ar ru zh pt");

		localeURLFilter.addMappingForUrlPatterns(null, false, "/*");

		// Hazelcast web filter configuration
		FilterRegistration.Dynamic hazelcastWebFilter = servletContext
				.addFilter("hazelcastWebFilter", new DelegatingFilterProxy("hazelcastWebFilter"));

		hazelcastWebFilter.setInitParameter("targetFilterLifecycle", "true");

		hazelcastWebFilter.addMappingForUrlPatterns(
				EnumSet.of(DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.REQUEST), false, "/*");


		// EnvVar filter configuration
		// Adds variables to all requests
		FilterRegistration.Dynamic envVarFilter = servletContext
				.addFilter("envVarFilter", new DelegatingFilterProxy("envVarFilter"));

		envVarFilter.addMappingForUrlPatterns(null, false, "/*");

		// Genesys Web API Filter configuration
		FilterRegistration.Dynamic webApiFilter = servletContext
				.addFilter("webApiFilter", new DelegatingFilterProxy("webApiFilter"));

		webApiFilter.addMappingForUrlPatterns(null, false, "/webapi/*");

		// Sitemesh filter configuration
		FilterRegistration.Dynamic sitemeshFilter = servletContext
				.addFilter("sitemesh", ConfigurableSiteMeshFilter.class);

		sitemeshFilter.addMappingForUrlPatterns(null, false, "/*");

		// New GUI filter configuration
		FilterRegistration.Dynamic newGUIFilter = servletContext
				.addFilter("newGUIFilter", NewGUIFilter.class);

		newGUIFilter.setInitParameter("exclude-paths", "/html /login-attempt");

		newGUIFilter.addMappingForUrlPatterns(null, false, "/*");
	}
}
