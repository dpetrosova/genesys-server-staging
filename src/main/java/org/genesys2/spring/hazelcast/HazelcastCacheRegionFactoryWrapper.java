/*
 * Copyright (c) 2008-2013, Hazelcast, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.genesys2.spring.hazelcast;

import java.util.Properties;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.CollectionRegion;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.NaturalIdRegion;
import org.hibernate.cache.spi.QueryResultsRegion;
import org.hibernate.cache.spi.RegionFactory;
import org.hibernate.cache.spi.TimestampsRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cfg.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.hibernate.HazelcastCacheRegionFactory;

@Component
public class HazelcastCacheRegionFactoryWrapper implements RegionFactory {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7713402428018035618L;
	private static HazelcastCacheRegionFactory hazelcastCacheRegionFactory;

	public HazelcastCacheRegionFactoryWrapper() {

	}

	public HazelcastCacheRegionFactoryWrapper(final Properties properties) {
		this();
	}

	@Autowired(required = true)
	public void setHazelcastCacheRegionFactory(HazelcastCacheRegionFactory hazelcastCacheRegionFactory) {
		HazelcastCacheRegionFactoryWrapper.hazelcastCacheRegionFactory = hazelcastCacheRegionFactory;
	}

	@Override
	public CollectionRegion buildCollectionRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
		return hazelcastCacheRegionFactory.buildCollectionRegion(regionName, properties, metadata);
	}

	@Override
	public void start(Settings settings, Properties properties) throws CacheException {
		hazelcastCacheRegionFactory.start(settings, properties);
	}

	@Override
	public void stop() {
		hazelcastCacheRegionFactory.stop();
	}

	@Override
	public boolean isMinimalPutsEnabledByDefault() {
		return hazelcastCacheRegionFactory.isMinimalPutsEnabledByDefault();
	}

	@Override
	public AccessType getDefaultAccessType() {
		return hazelcastCacheRegionFactory.getDefaultAccessType();
	}

	@Override
	public long nextTimestamp() {
		return hazelcastCacheRegionFactory.nextTimestamp();
	}

	@Override
	public EntityRegion buildEntityRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
		return hazelcastCacheRegionFactory.buildEntityRegion(regionName, properties, metadata);
	}

	@Override
	public NaturalIdRegion buildNaturalIdRegion(String regionName, Properties properties, CacheDataDescription metadata) throws CacheException {
		return hazelcastCacheRegionFactory.buildNaturalIdRegion(regionName, properties, metadata);
	}

	@Override
	public QueryResultsRegion buildQueryResultsRegion(String regionName, Properties properties) throws CacheException {
		return hazelcastCacheRegionFactory.buildQueryResultsRegion(regionName, properties);
	}

	@Override
	public TimestampsRegion buildTimestampsRegion(String regionName, Properties properties) throws CacheException {
		return hazelcastCacheRegionFactory.buildTimestampsRegion(regionName, properties);
	}

}
