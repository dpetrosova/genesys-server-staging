#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=غیرمجاز
http-error.401.text=باید وارد سیستم شوید.
http-error.403=دسترسی مجاز نیست
http-error.403.text=شما اجازه دسترسی به این منبع را ندارید.
http-error.404=یافت نشد
http-error.404.text=منبع درخواستی یافت نشد اما ممکن است در آینده موجود باشد.
http-error.500=خطای داخلی سرور
http-error.500.text=وضعیت غیرمنتظره‌ای پیش آمده و هیچ پیام خاص دیگری مناسب این وضعیت نیست.
http-error.503=سرویس در دسترس نیست
http-error.503.text=سرور در حال حاضر در دسترس نیست (به علت وارد آمدن بار بیش از حد یا خاموشی برای سرویس و نگه‌داری).


# Login
login.username=نام کاربری
login.password=رمز عبور
login.invalid-credentials=نام کاربری یا رمز عبور اشتباه است.
login.remember-me=ذخیره مشخصات ورود
login.login-button=ورود به سیستم
login.register-now=ایجاد اشتراک
logout=خروج از سیستم
login.forgot-password=رمز عبور خود را فراموش کرده‌اید؟
login.with-google-plus=ورود با ‎Google+‎

# Registration
registration.page.title=ایجاد اشتراک کاربری
registration.title=برای خود اشتراک ایجاد کنید
registration.invalid-credentials=نام کاربری یا رمز عبور اشتباه است.
registration.user-exists=نام کاربری قبلاً استفاده شده است.
registration.email=ایمیل
registration.password=رمز عبور
registration.confirm-password=تکرار رمز عبور
registration.full-name=نام کامل شما
registration.create-account=ایجاد اشتراک
captcha.text=متن امنیتی


id=شناسه

name=نام
description=شرح
actions=اقدامات
add=افزودن
edit=ویرایش
save=ذخیره
create=ایجاد
cancel=لغو
delete=حذف
prompt.confirm-delete=آیا واقعاً باید سابقه منتخب را حذف کنیم؟

jump-to-top=بازگشت به بالا\!

pagination.next-page=< بعدی
pagination.previous-page=قبلی >


# Language
locale.language.change=تغییر مکان
i18n.content-not-translated=این محتوا به زبان شما موجود نیست. لطفاً اگر می‌توانید به ترجمه آن کمک کنید با ما تماس بگیرید\!

data.error.404=اطلاعات درخواستی در سیستم یافت نشد.
page.rendertime=پردازش این صفحه {0} میلی‌ثانیه طول کشید.

footer.copyright-statement=‎&copy; 2013 - 2015 Data Providers and the Crop Trust

menu.home=صفحه اصلی
menu.browse=مرور
menu.datasets=داده‌های C&E
menu.descriptors=توصیف‌گرها
menu.countries=کشورها
menu.institutes=مؤسسات
menu.my-list=فهرست من
menu.about=درباره Genesys
menu.contact=تماس با ما
menu.disclaimer=سلب مسئولیت
menu.feedback=بازخورد
menu.help=راهنما
menu.terms=شرایط و ضوابط استفاده
menu.copying=خط مشی کپی‌رایت
menu.privacy=خط مشی حریم خصوصی
menu.newsletter=خبرنامه Genesys
menu.join-the-community=به جامعه Genesys بپیوندید
menu.faq=پرسش‌های متداول
menu.news=اخبار
page.news.title=اخبار
page.news.intro=آخرین خبرها از سراسر اجتماع Genesys – از جزئیات مربوط به جدیدترین اعضا تا به‌روزرسانی‌های مربوط به رکوردهای در حال نگهداری در سراسر دنیا.

# Extra content
menu.what-is-genesys=Genesys چیست؟
menu.who-uses-genesys=چه کسی از Genesys استفاده می‌کند؟
menu.how-to-use-genesys=چگونه از Genesys استفاده کنیم؟
menu.history-of-genesys=تاریخچه Genesys
menu.about-genesys-data=درباره داده‌های Genesys



page.home.title=Genesys PGR

user.pulldown.administration=مدیریت
user.pulldown.users=فهرست کاربر
user.pulldown.logout=خروج از سیستم
user.pulldown.profile=نمایه من
user.pulldown.oauth-clients=مشتریان OAuth
user.pulldown.teams=تیم‌ها
user.pulldown.manage-content=مدیریت محتوا

user.pulldown.heading={0}
user.create-new-account=ایجاد اشتراک
user.full-name=نام کامل
user.email=آدرس ایمیل
user.account-status=وضعیت اشتراک
user.account-disabled=اشتراک غیرفعال شده است
user.account-locked-until=اشتراک قفل شده است تا
user.roles=نقش‌های کاربری
userprofile.page.title=نمایه کاربر
userprofile.page.intro=Genesys به عنوان یک اجتماع، به موفقیت کاربران خویش تکیه دارد. چه یک محقق منفرد باشید و چه یک مؤسسه بزرگ را نمایندگی کنید، می‌توانید نمایه خود را در اینجا به‌روز کنید تا نمایانگر علایق و داده‌های موجود در مجموعه شما باشد.
userprofile.update.title=به‌روزرسانی نمایه

user.page.list.title=اشتراک‌های کاربری ثبت شده

crop.croplist=فهرست فراورده‌های کشاورزی
crop.all-crops=تمام فراورده‌ها
crop.taxonomy-rules=قواعد طبقه‌بندی
crop.view-descriptors=مشاهده توصیف‌گرهای فراورده کشاورزی...
crop.page.edit.title=ویرایش {0}
crop.summary=خلاصه (فراداده HTML)

activity.recent-activity=فعالیت اخیر

country.page.profile.title=نمایه کشور\: {0}
country.page.list.title=فهرست کشور
country.page.list.intro=با کلیک بر روی نام یک کشور، می‌توانید مؤسسات ثبت شده در آن کشور و نیز آمار مربوط به ذخایر ژنتیکی گیاهی موجود در آن کشور را به طور کلی ملاحظه نمایید.
country.page.not-current=این مدخل مربوط به گذشته است.
country.page.faoInstitutes={0} موسسه در WIEWS ثبت شده است
country.stat.countByLocation={0} رکورد در مؤسسات این کشور
country.stat.countByOrigin={0} مورد از رکوردهای ثبت شده در Genesys به این کشور مربوط است.
country.statistics=آمار کشوری
country.accessions.from=رکوردهای گردآوری شده در {0}
country.more-information=اطلاعات بیشتر\:
country.replaced-by=کد کشور با {0} جایگزین می‌شود
country.is-itpgrfa-contractingParty={0} یکی از طرفین «پیمان بین‌المللی ذخایر ژنتیکی گیاهی در زمینه غذا و کشاورزی» (ITPGRFA) است.
select-country=انتخاب کشور

project.page.list.title=پروژه‌ها
project.page.profile.title={0}
project.code=کد پروژه
project.name=نام پروژه
project.url=وب‌سایت پروژه
project.summary=خلاصه (فراداده HTML)
project.accessionLists=فهرست‌های رکورد


faoInstitutes.page.list.title=مؤسسات WIEWS
faoInstitutes.page.profile.title=WIEWS ‏{0}
faoInstitutes.stat.accessionCount=رکوردها در Genesys\:
faoInstitutes.stat.datasetCount=مجموعه داده‌های بیشتر در Genesys\:
faoInstitute.stat-by-crop=فراورده‌هایی که بیشتر به نمایش در آمده است
faoInstitute.stat-by-genus=سرده‌هایی که بیشتر به نمایش در آمده است
faoInstitute.stat-by-species=گونه‌هایی که بیشتر به نمایش در آمده است
faoInstitute.accessionCount={0} رکورد
faoInstitute.statistics=آمار مؤسسه
faoInstitutes.page.data.title=رکوردهای موجود در {0}
faoInstitute.accessions.at=رکوردهای موجود در {0}
faoInstitutes.viewAll=مشاهده تمام مؤسسات ثبت شده
faoInstitutes.viewActiveOnly=مشاهده مؤسساتی که در Genesys رکورد دارند
faoInstitute.no-accessions-registered=لطفاً اگر می‌توانید در گردآوری داده از این مؤسسه کمک کنید با ما تماس بگیرید.
faoInstitute.institute-not-current=این رکورد بایگانی شده است.
faoInstitute.view-current-institute=پیمایش به {0}.
faoInstitute.country=کشور
faoInstitute.code=کد WIEWS
faoInstitute.email=آدرس ایمیل
faoInstitute.acronym=سرنام
faoInstitute.url=پیوند وب
faoInstitute.summary=خلاصه (فراداده HTML)
faoInstitute.member-of-organizations-and-networks=سازمان‌ها و شبکه‌ها\:
faoInstitute.uniqueAcceNumbs.true=هر شماره رکورد در این مؤسسه منحصر به فرد است.
faoInstitute.uniqueAcceNumbs.false=ممکن است از یک شماره رکورد در مجموعه‌های جداگانه‌ای در این مؤسسه استفاده شود.
faoInstitute.requests.mailto=آدرس ایمیل برای درخواست‌های مواد\:
faoInstitute.allow.requests=درخواست مواد مجاز باشد
faoInstitute.sgsv=کد SGSV
faoInstitute.data-title=پایگاه داده اطلاعات مربوط به رکوردها در {0} نگهداری می‌شود
faoInstitute.overview-title=مرور کلی اطلاعات مربوط به رکوردها در {0} نگهداری می‌شود
faoInstitute.datasets-title=داده‌های مربوط به ارزیابی و مشخصات توسط {0} ارائه می‌شود
faoInstitute.meta-description=مرور کلی منابع ذخایر ژنتیکی گیاهی در گردآوری‌ها در {0} ({1}) بانک ژن نگهداری می‌شود
faoInstitute.link-species-data=اطلاعات مربوط به رکوردهای {2} را در {0} ({1}) مشاهده کنید
faoInstitute.wiewsLink=جزئیات {0} در وب‌سایت FAO WIEWS

view.accessions=مرور در رکوردها
view.datasets=مشاهده مجموعه داده‌ها
paged.pageOfPages=صفحه {0} از {1}
paged.totalElements={0} مدخل

accessions.number={0} رکورد
accession.metadatas=داده‌های C&E
accession.methods=داده‌های مربوط به ارزیابی و مشخصات
unit-of-measure=واحد اندازه‌گیری

ce.trait=ویژگی
ce.sameAs=مشابه
ce.methods=روش‌ها
ce.method=روش
method.fieldName=فیلد پایگاه داده

accession.uuid=UUID
accession.accessionName=شماره رکورد
accession.acceNumb=شماره رکورد
accession.origin=کشور مبدأ
accession.orgCty=کشور مبدأ
accession.holdingInstitute=مؤسسه دارنده
accession.institute.code=مؤسسه دارنده
accession.holdingCountry=موقعیت
accession.institute.country.iso3=موقعیت
accession.taxonomy=نام علمی
accession.taxonomy.sciName=نام علمی
accession.genus=سرده
accession.taxonomy.genus=سرده
accession.species=گونه
accession.taxonomy.species=گونه
accession.subtaxa=Subtaxa
accession.taxonomy.subtaxa=Subtaxa
accession.crop=نام فراورده کشاورزی
accession.crops=نام فراورده کشاورزی
accession.otherNames=نام دیگر
accession.inTrust=به صورت امانی
accession.mlsStatus=وضعیت MLS
accession.duplSite=مؤسسه مربوط به کپی ایمنی
accession.inSvalbard=به منظور رعایت ایمنی، کپی در اسوالبارد نگه‌داری شده است
accession.inTrust.true=این رکورد به موجب «ماده 15 پیمان بین‌المللی ذخایر ژنتیکی گیاهی در زمینه غذا و کشاورزی» تهیه شده است.
accession.mlsStatus.true=این رکورد در «سیستم چندجانبه ITPGRFA» وجود دارد.
accession.inSvalbard.true=به منظور رعایت ایمنی، کپی در «خزانه جهانی بذر اسوالبارد» نگه‌داری شده است.
accession.not-available-for-distribution=این رکورد برای توزیع موجود نیست.
accession.this-is-a-historic-entry=این یک سابقه مربوط به گذشته از یک رکورد است.
accession.historic=مدخل مربوط به گذشته
accession.available-for-distribution=این رکورد برای توزیع موجود است.
accession.elevation=ارتفاع
accession.geolocation=موقعیت جغرافیایی (عرض، طول)

accession.storage=نوع ذخیره ژرم‌پلاسم
accession.storage.=
accession.storage.10=گردآوری بذر
accession.storage.11=گردآوری بذر کوتاه مدت
accession.storage.12=گردآوری بذر میان‌مدت
accession.storage.13=گردآوری بذر بلندمدت
accession.storage.20=گردآوری از کشتزار
accession.storage.30=گردآوری آزمایشگاهی
accession.storage.40=گردآوری به روش سردنگه‌داری
accession.storage.50=گردآوری DNA
accession.storage.99=سایر موارد

accession.breeding=اطلاعات پرورش‌دهنده
accession.breederCode=کد پرورش‌دهنده
accession.pedigree=شجره‌نامه
accession.collecting=اطلاعات گردآوری
accession.collecting.site=موقعیت محل گردآوری
accession.collecting.institute=مؤسسه گردآورنده
accession.collecting.number=شماره گردآوری
accession.collecting.date=تاریخ گردآوری نمونه
accession.collecting.mission=شناسه مأموریت گردآوری
accession.coll.collMissId=شناسه مأموریت گردآوری
accession.collecting.source=منبع گردآوری/اکتساب

accession.collectingSource.=
accession.collectingSource.10=زیستگاه وحشی
accession.collectingSource.11=جنگل یا بیشه‌زار
accession.collectingSource.12=بوته‌زار
accession.collectingSource.13=علف‌زار
accession.collectingSource.14=بیابان یا توندرا
accession.collectingSource.15=زیستگاه آبی
accession.collectingSource.20=زیستگاه پرورشی یا کشتزار
accession.collectingSource.21=کشتزار
accession.collectingSource.22=باغستان
accession.collectingSource.23=حیاط خلوت، آشپزخانه یا باغچه خانه (شهری، حومه شهری یا روستایی)
accession.collectingSource.24=زمین آیش
accession.collectingSource.25=مرغزار
accession.collectingSource.26=فروشگاه محصولات کشاورزی
accession.collectingSource.27=زمین خرمن‌کوبی
accession.collectingSource.28=پارک
accession.collectingSource.30=بازار یا فروشگاه
accession.collectingSource.40=مؤسسه، پایگاه آزمایشی، سازمان پژوهشی، بانک ژن
accession.collectingSource.50=شرکت تولید بذر
accession.collectingSource.60=زیستگاه بایر، تخریب شده یا هرزعلف
accession.collectingSource.61=کنار جاده
accession.collectingSource.62=حاشیه کشتزار
accession.collectingSource.99=سایر موارد

accession.donor.institute=مؤسسه اهداکننده
accession.donor.accessionNumber=شناسه رکورد اهداکننده
accession.geo=اطلاعات جغرافیایی
accession.geo.datum=مبنای مختصات
accession.geo.method=روش ارجاع جغرافیایی
accession.geo.uncertainty=احتمال خطا در مختصات
accession.geo.latitudeAndLongitude=موقعیت جغرافیایی

accession.sampStat=وضعیت زیست‌شناختی رکورد
accession.sampleStatus=وضعیت زیست‌شناختی رکورد
accession.sampleStatus.=
accession.sampleStatus.100=وحشی
accession.sampleStatus.110=طبیعی
accession.sampleStatus.120=نیمه طبیعی/وحشی
accession.sampleStatus.130=نیمه‌طبیعی/بذرپاشی شده
accession.sampleStatus.200=هرزگیاه
accession.sampleStatus.300=لندریس/کولتیوار سنتی
accession.sampleStatus.400=مطالب مربوط به پژوهش/پرورش
accession.sampleStatus.410=خط پرورش‌دهندگان
accession.sampleStatus.411=جمعیت ساختگی
accession.sampleStatus.412=دورگه
accession.sampleStatus.413=جمعیت پایه/پیش‌گونه بنیان‌گذار
accession.sampleStatus.414=خط درون‌زاد
accession.sampleStatus.415=جمعیت تفکیک‌کننده
accession.sampleStatus.416=گزیده کلونی
accession.sampleStatus.420=پیش‌گونه ژنتیکی
accession.sampleStatus.421=جهش‌یافته
accession.sampleStatus.422=پیش‌گونه‌های سیتوژنتیکی
accession.sampleStatus.423=سایر پیش‌گونه‌های ژنتیکی
accession.sampleStatus.500=کولتیوار بهبودیافته/پیشرفته
accession.sampleStatus.600=GMO
accession.sampleStatus.999=سایر موارد

accession.availability=موجودی برای توزیع
accession.aliasType.ACCENAME=نام رکورد
accession.aliasType.DONORNUMB=شناسه رکورد اهداکننده
accession.aliasType.BREDNUMB=نامی که پرورش‌دهنده مشخص کرده است
accession.aliasType.COLLNUMB=شماره گردآوری
accession.aliasType.OTHERNUMB=سایر نام‌ها
accession.aliasType.DATABASEID=(شناسه پایگاه داده)
accession.aliasType.LOCALNAME=(نام محلی)

accession.availability.=نامشخص
accession.availability.true=موجود
accession.availability.false=موجود نیست

accession.historic.true=مربوط به گذشته
accession.historic.false=فعال
accession.acceUrl=URL تکمیلی رکورد
accession.scientificName=نام علمی

accession.page.profile.title=نمایه رکورد\: {0}
accession.page.resolve.title=چند رکورد یافت شد
accession.resolve=چند رکورد با نام «{0}» در Genesys یافت شد. یکی از موارد را در فهرست انتخاب کنید.
accession.page.data.title=مرورگر رکوردها
accession.page.data.intro=با استفاده از Genesys در داده‌های مربوط به رکوردها به کاوش بپردازید. برای بهبود جستجوی خویش، فیلترهایی مانند کشور مبدا، فرآورده کشاورزی، گونه‌ها یا طول و عرض جغرافیایی محل جمع‌آوری را اضافه کنید.
accession.taxonomy-at-institute=مشاهده {0} در {1}

accession.svalbard-data=داده‌های مربوط به کپی‌برداری در «خزانه جهانی بذر اسوالبارد»
accession.svalbard-data.taxonomy=نام علمی گزارش شده
accession.svalbard-data.depositDate=تاریخ سپردن
accession.svalbard-data.boxNumber=شماره جعبه
accession.svalbard-data.quantity=تعداد/مقدار
accession.remarks=ملاحظات

accession.imageGallery=تصاویر رکوردها

taxonomy.genus=سرده
taxonomy.species=گونه
taxonomy.taxonName=نام علمی
taxonomy-list=فهرست طبقه‌بندی

selection.checkbox=برای افزودن یا حذف رکورد از لیست خود کلیک کنید
selection.page.title=رکوردهای انتخاب شده
selection.page.intro=با کاوش در میلیون‌ها رکورد نگهداری شده در Genesys، می‌توانید فهرست خودتان را ایجاد کنید تا بتوانید نتایج جستجوی خود را ردگیری نمایید. انتخاب‌های شما در اینجا ذخیره می‌شوند تا بتوانید در هر زمان دلخواه به آنها برگردید.
selection.add=افزودن {0} به فهرست
selection.remove=حذف {0} از فهرست
selection.clear=پاک کردن فهرست
selection.reload-list=بارگذاری مجدد فهرست
selection.map=نمایش نقشه رکورد
selection.send-request=ارسال درخواست برای ژرم‌پلاسم
selection.empty-list-warning=شما هیچ رکوردی را به فهرست اضافه نکرده‌اید.
selection.add-many=افزودن چند رکورد
selection.add-many.accessionIds=فهرست کردن شماره‌های رکورد (ACCENUMB) آن‌گونه که در Genesys ثبت شده است، با جداسازی توسط ویرگول، نقطه ویرگول یا سطر جدید.
selection.add-many.instCode=کد WIEWS مؤسسه دارنده
selection.add-many.button=افزودن به فهرست انتخاب

savedmaps=ذخیره نقشه فعلی
savedmaps.list=فهرست نقشه
savedmaps.save=ذخیره نقشه
taxonomy.subtaxa=Subtaxa

filter.enter.title=وارد کردن عنوان فیلتر
filters.page.title=فیلترهای داده
filters.view=فیلترهای فعلی
filter.filters-applied=شما از فیلتر استفاده کرده‌اید.
filter.filters-not-applied=می‌توانید داده‌ها را فیلتر کنید.
filters.data-is-filtered=داده‌ها فیلتر شده است.
filters.toggle-filters=فیلترها
filter.taxonomy=نام علمی
filter.art15=رکورد مربوط به ماده 15 پیمان ITPGRFA
filter.acceNumb=شماره رکورد
filter.seqNo=شماره ترتیبی شناسایی شده
filter.alias=نام رکورد
filter.crops=نام فراورده
filter.orgCty.iso3=کشور مبدأ
filter.institute.code=نام مؤسسه دارنده
filter.institute.country.iso3=کشور مؤسسه دارنده
filter.sampStat=وضعیت زیست‌شناختی رکورد
filter.institute.code=نام مؤسسه دارنده
filter.geo.latitude=عرض جغرافیایی
filter.geo.longitude=طول جغرافیایی
filter.geo.elevation=ارتفاع
filter.taxonomy.genus=سرده
filter.taxonomy.species=گونه
filter.taxonomy.subtaxa=Subtaxa
filter.taxSpecies=گونه
filter.taxonomy.sciName=نام علمی
filter.sgsv=به منظور رعایت ایمنی، کپی در اسوالبارد نگه‌داری شده است
filter.mlsStatus=وضعیت MLS رکورد
filter.available=موجود برای توزیع
filter.historic=سابقه مربوط به گذشته
filter.donorCode=مؤسسه اهداکننده
filter.duplSite=محل نگه‌داری کپی ایمنی
filter.download-dwca=دانلود ZIP
filter.download-mcpd=دانلود MCPD
filter.add=بهبود جستجو بر اساس رده‌بندی
filter.additional=بهبود جستجو بر اساس ویژگی
filter.apply=اعمال
filter.close=بستن
filter.remove=حذف فیلتر
filter.autocomplete-placeholder=بیش از 3 کاراکتر تایپ کنید...
filter.coll.collMissId=شناسه مأموریت گردآوری
filter.storage=نوع ذخیره ژرم‌پلاسم
filter.string.equals=مساوی است با
filter.string.like=شروع می‌شود با
filter.inverse=مستثنی کردن
filter.set-inverse=مستثنی کردن مقادیر منتخب
filter.internal.message.like=مانند {0}
filter.internal.message.between=بین {0}
filter.internal.message.and={0} و {0}
filter.internal.message.more=بیشتر از {0}
filter.internal.message.less=کمتر از {0}
filter.lists=فهرست شده در فهرست رکورد

columns.add=تغییر ستون‌های نمایش داده شده
columns.apply=اعمال
columns.availableColumns=ستون‌هایی که باید نمایش داده شوند را انتخاب کنید و روی "اعمال" کلیک نمایید
columns.latitudeAndLongitude=عرض جغرافیایی و طول جغرافیایی
columns.collectingMissionID=شناسه مأموریت گردآوری

columns.acceNumb=شماره رکورد
columns.scientificName=نام علمی
columns.orgCty=کشور مبدأ
columns.sampStat=وضعیت زیست‌شناختی رکورد
columns.instCode=مؤسسه دارنده

search.page.title=جستجو در متن کامل
search.no-results=هیچ موردی مطابق با جستار مورد نظر شما یافت نشد.
search.input.placeholder=جستجو در Genesys...
search.search-query-missing=لطفاً جستار جستجوی خود را وارد کنید.
search.search-query-failed=متأسفانه جستجو به علت خطای {0} انجام نشد
search.button.label=جستجو
search.add-genesys-opensearch=جستجوی Genesys را در مرورگر خود ثبت کنید
search.section.accession=رکوردها
search.section.article=محتوا
search.section.activitypost=آیتم‌های اخبار
search.section.institute=بانک‌های ژن
search.section.country=کشورها

admin.page.title=مدیریت Genesys 2
metadata.page.title=مجموعه داده‌ها
metadata.page.intro=مجموعه داده‌های مربوط به ارزیابی و مشخصات که توسط محققان و مؤسسات تحقیقاتی به Genesys آپلود شده‌اند را مرور نمایید. برای هریک از آنها، می‌توانید داده‌ها را با فرمت Excel یا CVS دانلود کنید.
metadata.page.view.title=جزئیات مجموعه داده
metadata.download-dwca=دانلود ZIP
page.login=ورود به سیستم

traits.page.title=توصیف‌گرها
trait-list=توصیف‌گرها


organization.page.list.title=سازمان‌ها و شبکه‌ها
organization.page.profile.title={0}
organization.slug=سرنام سازمان یا شبکه
organization.title=نام کامل
organization.summary=خلاصه (فراداده HTML)
filter.institute.networks=شبکه


menu.report-an-issue=اعلام مشکل
menu.scm=کد منبع
menu.translate=Genesys را ترجمه کنید

article.edit-article=ویرایش مقاله
article.slug=نام کوتاه مقاله (URL)
article.title=عنوان مقاله
article.body=متن مقاله
article.summary=خلاصه (فراداده HTML)
article.post-to-transifex=ارسال مطلب به Transifex
article.remove-from-transifex=حذف از Transifex
article.fetch-from-transifex=اخذ ترجمه‌ها
article.transifex-resource-updated=این منبع با موفقیت در Transifex به‌روزرسانی شد.
article.transifex-resource-removed=این منبع با موفقیت از Transifex حذف شد.
article.transifex-failed=هنگام تبادل داده‌ها با Transifex خطایی رخ داد
article.translations-updated=منبع با موفقیت با داده‌های ترجمه شده به‌روز گردید\!
article.share=اشتراک‌گذاری این مقاله

user.accession.list.saved-updated=فهرست رکوردهای شما با موفقیت ذخیره شد.
user.accession.list.deleted=فهرست رکوردهای شما با موفقیت از سرور حذف شد.
user.accession.list.create-update=ذخیره
user.accession.list.delete=حذف
user.accession.list.title=فهرست رکورد

activitypost=مطلب مربوط به فعالیت
activitypost.add-new-post=افزودن مطلب جدید
activitypost.post-title=عنوان مطلب
activitypost.post-body=متن

blurp.admin-no-blurp-here=شرحی در اینجا وجود ندارد.
blurp.blurp-title=عنوان شرح
blurp.blurp-body=محتوای شرح
blurp.update-blurp=ذخیره شرح


oauth2.confirm-request=تایید دسترسی
oauth2.confirm-client=شما، <b>{0}</b>، بدین وسیله به <b>{1}</b> اجازه می‌دهید به ذخایر حفاظت‌شده شما دسترسی داشته باشد.
oauth2.button-approve=بله، دسترسی مجاز است
oauth2.button-deny=خیر، دسترسی مجاز نیست

oauth2.authorization-code=کد شناسایی
oauth2.authorization-code-instructions=این کد شناسایی را کپی کنید\:

oauth2.access-denied=دسترسی منع شده است
oauth2.access-denied-text=شما دسترسی به ذخایر خود را منع کرده‌اید.

oauth-client.page.list.title=مشتریان OAuth2
oauth-client.page.profile.title=مشتری OAuth2\:‏ {0}
oauth-client.active-tokens=فهرست ژتون‌های صادره
client.details.title=عنوان مشتری
client.details.description=شرح

maps.loading-map=در حال بارگذاری نقشه...
maps.view-map=مشاهده نقشه
maps.accession-map=نقشه رکورد
maps.accession-map.intro=نقشه مربوطه، محل گردآوری رکوردهای حاوی ارجاعات جغرافیایی را نشان می‌دهد.
maps.baselayer.list=تغییر لایه مبنای نقشه

audit.createdBy=ایجاد شده توسط {0}
audit.lastModifiedBy=آخرین به‌روزرسانی توسط {0}

itpgrfa.page.list.title=طرفین ITPGRFA

request.page.title=درخواست مواد از مؤسسات دارنده
request.total-vs-available=از میان {0} رکورد موجود در فهرست، {1} مورد برای توزیع موجود است.
request.start-request=درخواست ژرم‌پلاسم موجود
request.your-email=آدرس ایمیل شما\:
request.accept-smta=پذیرش SMTA/MTA\:
request.smta-will-accept=شرایط و ضوابط SMTA/MTA را می‌پذیرم
request.smta-will-not-accept=شرایط و ضوابط SMTA/MTA را نمی‌پذیرم
request.smta-not-accepted=شما مشخص نکرده‌اید که شرایط و ضوابط SMTA/MTA را می‌پذیرید. به درخواست شما برای مواد رسیدگی نمی‌شود.
request.purpose=کاربرد مواد را مشخص کنید\:
request.purpose.0=سایر موارد (لطفاً در فیلد «ملاحظات» توضیح دهید)
request.purpose.1=پژوهش در زمینه غذا و کشاورزی
request.notes=سایر نظرات و ملاحظات خود را قید کنید\:
request.validate-request=تأیید اعتبار درخواست
request.confirm-request=تأیید رسید درخواست
request.validation-key=کلید تأیید اعتبار\:
request.button-validate=تأیید اعتبار
validate.no-such-key=کلید تأیید اعتبار معتبر نیست.

team.user-teams=تیم‌های کاربر
team.create-new-team=ایجاد تیم جدید
team.team-name=نام تیم
team.leave-team=خروج از تیم
team.team-members=اعضای تیم
team.page.profile.title=تیم\: {0}
team.page.list.title=همه تیم‌ها
validate.email.key=وارد کردن کلید
validate.email=تأیید اعتبار ایمیل
validate.email.invalid.key=کلید نامعتبر

edit-acl=ویرایش مجوزها
acl.page.permission-manager=مدیریت مجوزها
acl.sid=هویت امنیتی
acl.owner=مالک شیء
acl.permission.1=خواندن
acl.permission.2=نوشتن
acl.permission.4=ایجاد
acl.permission.8=حذف
acl.permission.16=مدیریت


ga.tracker-code=کد ردیاب GA

boolean.true=بله
boolean.false=خیر
boolean.null=نامشخص
userprofile.password=بازنشانی رمز عبور
userprofile.enter.email=ایمیل خود را وارد کنید
userprofile.enter.password=رمز عبور جدید را وارد کنید
userprofile.email.send=ارسال ایمیل

verification.invalid-key=کلید ژتون معتبر نیست.
verification.token-key=کلید تأیید اعتبار
login.invalid-token=ژتون دسترسی معتبر نیست

descriptor.category=رده‌بندی توصیف‌گر
method.coding-table=جدول کدگذاری

oauth-client.info=اطلاعات مشتری
oauth-client.list=فهرست مشتریان oauth
client.details.client.id=شناسه جزئیات مشتری
client.details.additional.info=اطلاعات بیشتر
client.details.token.list=فهرست ژتون‌ها
client.details.refresh-token.list=فهرست ژتون‌های رفرش
oauth-client.remove=حذف
oauth-client.remove.all=حذف همه
oauth-client=مشتری
oauth-client.token.issue.date=تاریخ صدور
oauth-client.expires.date=تاریخ انقضا
oauth-client.issued.tokens=ژتون‌های صادره
client.details.add=افزودن مشتری OAuth
oauth-client.create=ایجاد مشتری OAuth
oauth-client.id=شناسه مشتری
oauth-client.secret=رمز مشتری
oauth-client.redirect.uri=URI هدایت مجدد مشتری
oauth-client.access-token.accessTokenValiditySeconds=اعتبار ژتون دسترسی
oauth-client.access-token.refreshTokenValiditySeconds=اعتبار ژتون بازآوری
oauth-client.access-token.defaultDuration=استفاده از پیش‌فرض
oauth-client.title=عنوان مشتری
oauth-client.description=شرح
oauth2.error.invalid_client=شناسه مشتری معتبر نیست. اعتبار پارامترهای client_id و client_secret خود را تأیید کنید.

team.user.enter.email=ایمیل کاربر را وارد کنید
user.not.found=کاربر یافت نشد
team.profile.update.title=به‌روزرسانی اطلاعات تیم

autocomplete.genus=جستجوی سرده...

stats.number-of-countries={0} کشور
stats.number-of-institutes={0} مؤسسه
stats.number-of-accessions={0} رکورد
stats.number-of-historic-accessions={0} رکورد مربوط به گذشته

navigate.back=بازگشت


content.page.list.title=فهرست مقاله‌ها
article.lang=زبان
article.classPk=شیء

session.expiry-warning-title=هشدار در مورد انقضای جلسه
session.expiry-warning=جلسه فعلی شما رو به انقضاست. تمایل دارید این جلسه را تمدید کنید؟
session.expiry-extend=تمدید جلسه

download=دانلود
download.kml=دانلود KML


data-overview=مرور آماری
data-overview.intro=یک نمای آماری کلی از اطلاعات ذخیره شده در Genesys برای فیلترهای اعمال شده کنونی – همه چیز از تعداد کل رکوردها بر اساس کشور، تا تنوع گردآوری‌ها.
data-overview.short=مرور کلی
data-overview.institutes=مؤسسات دارنده
data-overview.composition=ترکیب هلدینگ‌های بانک ژن
data-overview.sources=منابع مواد
data-overview.management=مدیریت گردآوری
data-overview.availability=موجودی مواد
data-overview.otherCount=سایر موارد
data-overview.missingCount=نامشخص
data-overview.totalCount=مجموع
data-overview.donorCode=کد FAO WIEWS مؤسسه اهداکننده
data-overview.mlsStatus=موجود برای توزیع بر اساس MLS


admin.cache.page.title=مرور کلی نهانگاه برنامه کاربردی
cache.stat.map.ownedEntryCount=مدخل‌های نهانگاه تحت تملیک
cache.stat.map.lockedEntryCount=مدخل‌های نهانگاه قفل شده
cache.stat.map.puts=تعداد نوشتن‌ها در نهانگاه
cache.stat.map.hits=تعداد موارد منطبق نهانگاه

loggers.list.page=فهرست واقعه‌نگارهای پیکربندی شده
logger.add-logger=افزودن واقعه‌نگار
logger.edit.page=صفحه پیکربندی واقعه‌نگار
logger.name=نام واقعه‌نگار
logger.log-level=سطح واقعه‌نگاری
logger.appenders=الحاق‌گرهای واقعه‌نگاری

menu.admin.index=سرپرست
menu.admin.loggers=واقعه‌نگارها
menu.admin.caches=نهانگاه‌ها
menu.admin.ds2=مجموعه داده‌های DS2
menu.admin.usermanagement=مدیریت کاربر
menu.admin.repository=منبع
menu.admin.repository.files=مدیر فایل منبع
menu.admin.repository.galleries=گالری‌های تصویر

news.content.page.all.title=فهرست اخبار
news.archive.title=بایگانی اخبار

worldclim.monthly.title=وضعیت آب و هوا در محل گردآوری
worldclim.monthly.precipitation.title=بارش ماهانه
worldclim.monthly.temperatures.title=دماهای ماهانه
worldclim.monthly.precipitation=بارش ماهانه [میلی‌متر]
worldclim.monthly.tempMin=حداقل دما [‎°C]
worldclim.monthly.tempMean=میانگین دما [‎°C]
worldclim.monthly.tempMax=حداکثر دما [‎°C]

worldclim.other-climate=سایر داده‌های آب و هوایی

download.page.title=پیش از دانلود
download.download-now=دانلود شروع شود، منتظر خواهم ماند.

resolver.page.index.title=یافتن شناسه دائمی
resolver.acceNumb=شماره رکورد
resolver.instCode=کد مؤسسه دارنده
resolver.lookup=یافتن شناسه
resolver.uuid=UUID
resolver.resolve=رفع کردن

resolver.page.reverse.title=نتایج رفع کردن
accession.purl=URL دائمی

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=اجرای KPI
admin.kpi.executionrun.page=جزئیات مراحل اجرا

accession.pdci=شاخص تمامیت داده‌های پاسپورت
accession.pdci.jumbo=امتیاز PDCI\:‏ {0,number,0.00} از 10.0
accession.pdci.institute-avg=امتیاز PDCI میانگین برای این مؤسسه برابر است با ‎{0,number,0.00}‎
accession.pdci.about-link=مطالعه درباره شاخص تمامیت داده‌های پاسپورت
accession.pdci.independent-items=مستقل از نوع جمعیت
accession.pdci.dependent-items=وابسته به نوع جمعیت
accession.pdci.stats-text=امتیاز PDCI میانگین برای رکوردهای {0} معادل ‎{1,number,0.00}‎ می‌باشد، که دارای حداقل امتیاز ‎{2,number,0.00}‎ و حداکثر امتیاز ‎{3,number,0.00}‎ است.

accession.donorNumb=کد مؤسسه اهداکننده
accession.acqDate=تاریخ اکتساب

accession.svalbard-data.url=URL پایگاه داده Svalbard
accession.svalbard-data.url-title=اطلاعات سپرده در پایگاه داده SGSV
accession.svalbard-data.url-text=مشاهده اطلاعات سپرده SGSV مربوط به {0}

filter.download-pdci=دانلود داده‌های PDCI
statistics.phenotypic.stats-text=از بین {0} رکورد، {1} رکورد ‎({2,number,0.##%})‎ دارای حداقل یک ویژگی اضافی ثبت شده در یک مجموعه داده موجود در Genesys (به‌طور میانگین ‎{3,number,0.##}‎ ویژگی در ‎{4,number,0.##}‎ مجموعه) می‌باشد.

twitter.tweet-this=توییت کنید\!
twitter.follow-X=دنبال کردن ‎@{0}‎
linkedin.share-this=اشتراک‌گذاری در LinkedIn
share.link=اشتراک‌گذاری پیوند
share.link.placeholder=لطفاً صبر کنید...
share.link.text=لطفاً از نسخه کوتاه شده URL کامل این صفحه استفاده کنید\:

welcome.read-more=درباره Genesys بیشتر بدانید
twitter.latest-on-twitter=آخرین موارد در توییتر
video.play-video=پخش ویدیو

heading.general-info=اطلاعات کلی
heading.about=درباره
heading.see-also=این موارد را نیز ملاحظه کنید
see-also.map=نقشه محل رکوردها
see-also.overview=نمای کلی گردآوری
see-also.country=اطلاعات بیشتر درباره حفظ PGR در {0}

help.page.intro=برای آگاهی از نحوه استفاده از Genesys بخش آموزش را ملاحظه کنید.

charts=نمودارها و نقشه‌ها
charts.intro=در اینجا می‌توانید نمودارها و نقشه‌های مفیدی را برای ارائه بعدی خود بیابید\!
chart.collections.title=اندازه مجموعه‌های بانک ژن
chart.attribution-text=www.genesys-pgr.org
chart.collections.series=تعداد رکوردها در بانک‌های ژن

label.load-more-data=بارگذاری بیشتر...

userlist.list-my-lists=فهرست‌های ذخیره شده رکوردها
userlist.title=عنوان فهرست
userlist.description=شرح فهرست
userlist.disconnect=قطع ارتباط فهرست
userlist.update-list=ذخیره فهرست به‌روز شده
userlist.make-new-list=ایجاد فهرست جدید
userlist.shared=اجازه به دیگران برای دسترسی به فهرست

region.page.list.title = مناطق جغرافیایی FAO
region.page.list.intro = فهرست‌های مناطق جغرافیایی FAO در زیر به شما امکان می‌دهند تا به داده‌های مربوط به رکوردهای جمع‌آوری شده یا نگهداری شده در منطقه مربوطه دسترسی داشته باشید.
region.page.show.parent = نمایش منطقه منشأ {0}
region.page.show.world = نمایش تمام مناطق دنیا
region.countries-in-region=فهرست کشورها در {0}
region.regions-in-region=مناطق FAO در {0}
region.show-all-regions=فهرست تمام مناطق FAO

repository.file.path=مسیر
repository.file.originalFilename=نام اصلی فایل
repository.file.title=عنوان
repository.file.subject=موضوع
repository.file.description=شرح
repository.file.creator=ایجادکننده
repository.file.created=تاریخ ایجاد
repository.file.rightsHolder=صاحب حقوق
repository.file.accessRights=حقوق دسترسی
repository.file.license=نام مجوز یا URL
repository.file.format=فرمت
repository.file.contentType=نوع محتوا
repository.file.extension=پسوند فایل
repository.file.extent=وسعت
repository.file.bibliographicCitation=نقل قول کتاب‌شناسی
repository.file.dateSubmitted=تاریخ ارائه
repository.file.lastModifiedDate=تاریخ آخرین اصلاح

file.upload-file=آپلود فایل
file.upload-file.help=فایل مربوط به آپلود را از کامپیوتر محلی انتخاب کنید

repository.gallery=گالری تصویر
repository.gallery.title=عنوان گالری تصویر\:
repository.gallery.description=شرح گالری تصویر\:
repository.gallery.path=مسیر مربوط به تصاویر گالری در منبع\:
repository.gallery.successfully-updated=گالری تصویر با موفقیت به‌روز شد\!
repository.gallery.removed=گالری تصویر حذف شد.
repository.gallery.create-here=گالری تصویر ایجاد کنید\!
repository.gallery.navigate=گالری تصویر را مشاهده کنید\!
repository.gallery.downloadImage=دانلود تصویر

welcome.search-genesys=جستجو در Genesys
welcome.networks=شبکه‌ها

