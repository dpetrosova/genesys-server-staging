Genesys and Svalbard Global Seed Vault
======================================
September 2016: Documentation commit {buildNumber}
:revnumber: {projectVersion}
:doctype: book
:toc: left
:toclevels: 5
:icons: font
:numbered:
:source-highlighter: pygments
:pygments-css: class
:pygments-linenums-mode: table


Genesys periodically synchronizes data from the http://www.nordgen.org/sgsv/[Svalbard Global Seed Vault] (**SGSV**) to link Genesys accessions with accessions backed up at Svalbard.

== SGSV database

SGSV database is a publicly accessible resource available at http://www.nordgen.org/sgsv/.

=== Seed samples database

The information on samples stored in SGSV is provided in `CSV` format at accessible at http://www.nordgen.org/sgsv/download.php?file=/scope/sgsv/files/sgsv_templates.tab[Seed samples download] link.

[cols="1,4", options="header"]
.SGSV Seed samples CSV format
|===
|Column header|Description
|sgsv_id|ID of the seed sample record in SGSV database. Unique.
|institute_code|FAO WIEWS Institute code (`INSTCODE`)
|deposit_box_number|Deposit box number. Numeric.
|collection_name|Collection name
|accession_number|Accession Number (`ACCENUMB`)
|full_scientific_name|Full scientific name of the accession
|country_of_collection_or_source|MCPD `ORIGCTY`
|number_of_seeds|Number of seeds. Count.
|regeneration_month_and_year|Information about regeneration date of sample.
|other_accession_designations|MCPD `OTHERNUMB`
|provider_institute_code|...
|accession_url|URL to detailed accession information (`ACCEURL`)
|country_code|
|country_name|
|continent_name|
|seeds|
|genus|MCPD `GENUS`
|species_epithet|MCPD `SPECIES`
|species|
|taxon_name|
|date_of_deposit|Date of deposit
|date_of_dataset|Date of dataset
|sgsv_template_id|
|box_id|Box ID reference.
|sgsv_taxon_id|Reference to SGSV taxonomy table
|taxon_authority|MCPD `SPAUTHOR`
|infraspesific_epithet|MCPD `SUBTAXA`
|vernacular_name|Crop name (MCPD `CROPNAME`)
|itis_tsn|
|sgsv_genus_id|Reference to SGSV taxonomy table
|accession_name|Accession name (`ACCENAME`)
|===


== Linking SGSV records with Genesys

Genesys attempts to link records based on the institute code (`INSTCODE`) and
accession number (`ACCENUMB`). Genebanks **must** use their assigned `INSTCODE` and the correct `ACCENUMB` from their datasets when they publish data on SGSV and on Genesys in order to appropriately link the records.

== Multiple deposits

Genesys gracefully handles multiple deposits of seed to SGSV for a single accession. Information on all seed deposits is rendered on the accession details page.
