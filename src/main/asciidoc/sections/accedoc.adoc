
[[accedoc]]
== Accession documentation in genebanks

Collections of PGRFA material in genebanks document at least the following for each accession

* <<accedoc-accenumb,Accession number>>
* Acquisition date `ACQDATE` when accession entered the collection
* <<accedoc-other,Other accession identifiers>>
* <<accedoc-tax,Taxonomy>>
* <<accedoc-storage,Storage and maintenance>>

A single accession is usually maintained as several individual *inventories* or lots, where each inventory
follows different management policies and is maintained in different conditions (e.g. cryo and in vitro, 
or base and active collection).

Inventory management is a topic of genebank collection management and is not further described here.


[[accedoc-accenumb]]
=== Accession number

Accession number is the unique identifier assigned to the material as it enters
the collection. This identifier generally has three components:

Prefix + Sequence number + Suffix

The *prefix* is commonly used to differentiate between different crop collections
maintained by the genebank. 

.Some prefixes used by http://www.iita.org[IITA] genebank
* `TMe` Cassava _Manihot esculenta_ collection
* `TVSu` Bambara groundnut _Vigna subterranea_ collection
* `TZm` Maize _Zea mays_ collection

*Sequence number* is assigned manually or by a computer system to ensure there are 
no duplicates. Some institutes prefer to zero-pad the number `00000102`.

The *suffix* allows differentiating samples of the same original material. A suffix 
might be used after making a selection from the original accession (e.g. a 
single seed descent) to be maintained as a separate sample. 
The exact meaning of the suffix is different for every institute.

[cols="1,1,1,2", options="header"] 
.Example accession numbers
|===
|Prefix|Sequence number|Suffix|Accession number
|TMe|419||TMe-419
|TVSu|13||TVSu-13
|===

[[accedoc-other]]
=== Other accession identifiers

Material enters the collection by collecting, from breeding programs,
or acquisition from other institutes. In each case, the material will already have some
identifier assigned by the collector, breeder or other institute.

*Accession name* is the vernacular name of the material and is commonly captured by
the collector or assigned by the breeder.


[[accedoc-coll]]
==== Collected material

Genebank accessions obtained through collecting missions should maintain data about the site and dates of the 
collecting and collector information.


[[accedoc-bred]]
==== Breeders material

Lines developed by breeding programs of the institute may be included the collection. Information provided by the breeders
should include the pedigree or ancestral information (selection history) of the material, along with names and identifiers used by the breeding
program and the codes and names of institutes that developed the material.

[[accedoc-acq]]
==== Acquisitions

Material coming from other institutes and genebanks must be accompanied by accession passport data
as documented in the source genebank.

NOTE: *Country of origin* is the country where the material was collected or bred, not the country of 
the source genebank.

Accession documentation should capture any identifiers provided by the source institute. This data
allows for validation and curation of passport data between the genebanks and allows researchers
to obtain material from either collection.



[[accedoc-tax]]
=== Taxonomy

Accession genus, species, species author, subtaxon and subtaxon authority are usually 
known, but are subject to change after expert identification or change in taxonomic system.

https://npgsweb.ars-grin.gov/gringlobal/taxon/abouttaxonomy.aspx[GRIN Taxonomy for Plants] and 
the http://mansfeld.ipk-gatersleben.de/[Mansfeld's World Database of Agriculture and Horticultural Crops] can serve for validating accession taxa.


[[accedoc-storage]]
=== Storage and maintenance

Ex situ genebanks maintain PGR material as seed, in the field, in vitro, cryo or in DNA collections.
Inventories (lots) of one accession may be managed by different methods (e.g. seed and cryo). 
See <<mcpd-storage,Storage>> in MCPD standard on how to capture multiple types of storage.


