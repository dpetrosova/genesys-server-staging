
[[other-standards]]
== Other relevant standards

[[iso-3166]]
=== ISO-3166 Country codes

https://en.wikipedia.org/wiki/ISO_3166[ISO-3166] standard defines 'Codes for the representation of 
names of countries and their subdivisions'.
https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3[ISO-3166-1 alpha-3] codes are three-letter country codes. The
Wikipedia page contains the listing of valid country codes. 
Genesys uses http://download.geonames.org/export/dump/countryInfo.txt as the source of ISO-3166 country codes. 

[[un-m49]]
=== UN M.49

UN defines standard country or area codes and geographical regions for statistical use:

* http://unstats.un.org/unsd/methods/m49/m49.htm
* http://unstats.un.org/unsd/methods/m49/m49alpha.htm
