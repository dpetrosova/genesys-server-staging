Genesys Server Reference Manual
===============================
December 2015: Documentation commit {buildNumber}
:revnumber: {projectVersion}
:doctype: book
:toc: left
:toclevels: 5
:icons: font
:numbered:
:source-highlighter: pygments
:pygments-css: class
:pygments-linenums-mode: table


[[intro]]
Introduction
------------

Genesys PGR (Plant Genetic Resources) is a free online global portal accessible at
link:$$https://www.genesys-pgr.org$$[www.genesys-pgr.org]
that allows the exploration of the world’s crop diversity through a single website.

This manual contains information required to configure, upgrade,  run and manage a Genesys Server.

////
NOTE: Note...

TIP: Pro tip...

IMPORTANT: Don't forget...

WARNING: Watch out for...

CAUTION: Ensure that...
////

[[download]]
=== Download

Binary *genesys2-server* distribution packages are available on request. Please contact helpdesk@genesys-pgr.org.  

[[sources]]
=== Sources

*genesys2-server* is licensed under link:$$http://www.apache.org/licenses/LICENSE-2.0.html$$[the Apache V2 license].

The source code of *genesys2-server* can be found on
  link:$$https://bitbucket.org/genesys2/genesys2-server$$[https://bitbucket.org/genesys2/genesys2-server]



include::genesys-properties.adoc[]
include::running-genesys.adoc[]
include::elasticsearch.adoc[]



Backup and Disaster Recovery
----------------------------


This section provides information required to properly backup Genesys application,
configuration and data for a safe and valid recovery.

:leveloffset: 1
include::sections/backup.adoc[]
include::sections/recovery.adoc[]
:leveloffset: 0


include::sections/wiews.adoc[]
include::sections/mcpd.adoc[]

include::api/security.adoc[]
include::api/accession.adoc[]
include::api/crop.adoc[]
include::api/requests.adoc[]

