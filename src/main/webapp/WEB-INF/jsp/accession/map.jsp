<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="maps.accession-map"/></title>
    <script type="text/javascript" src="<c:url value="/html/1/js/browse.js" />"></script>
    <script type="text/javascript" src="<c:url value="/explore/i18n.js"/>"></script>
</head>
<body class="map-page overview-page explore-page">
<cms:informative-h1 title="maps.accession-map" fancy="true" info="maps.accession-map.intro"/>

<div class="container-fluid">

    <div class="">
        <filters:filter-list availableFilters="${availableFilters}" filters="${filters}"
                             appliedFilters="${appliedFilters}" crops="${crops}" crop="${crop}"/>

        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 main-col-header">
            <div class="nav-header clearfix">

                <div class="pull-right list-view-controls">

                    <div class="btn-group" id="shareLink">
                        <a type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false" id="menuShareLink">
                            <span class="glyphicon glyphicon-share"></span>
                            <span><spring:message code="share.link"/></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="padding10">
                                <p><spring:message code="share.link.text"/></p>
                                <input id="shortLink" type="text"
                                       placeholder="<spring:message code="share.link.placeholder" />" value=""/>
                            </li>
                        </ul>
                    </div>

                    <a class="btn btn-default" id="exploreLink"
                       href="<c:url value="/explore"><c:param name="filter" value="${jsonFilter}" /></c:url>">
                        <span class="glyphicon glyphicon-list"></span>
                        <span style="margin-left: 0.5em;"><spring:message code="view.accessions"/></span></a>
                    <a class="btn btn-default" id="overviewLink"
                       href="<c:url value="/explore/overview"><c:param name="filter">${jsonFilter}</c:param></c:url>">
                        <span class="glyphicon glyphicon-eye-open"></span>
                        <span><spring:message code="data-overview.short"/></span>
                    </a>

                    <%--<a class="btn btn-default" href="<c:url value="/explore" />" id="selectArea">
                        <span class="glyphicon glyphicon-list"></span>
                        <span><spring:message code="view.accessions"/></span>
                    </a>--%>

                    <form style="display: inline-block" method="post" action="<c:url value="/explore/kml" />">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <input type="hidden" name="filter" value="<c:out value="${jsonFilter}" />"/>
                        <button class="btn btn-default" type="submit">
                            <span class="glyphicon glyphicon-save"></span>
                            <span><spring:message code="download.kml"/></span>
                        </button>
                    </form>
                </div>

                <div class="pull-left list-view-controls">
                    <a class="btn btn-default btn-back" href="">
                        <spring:message code="navigate.back"/></a>
                </div>

            </div>

        </div>


        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12" id="content-area">

            <!-- Modal -->
            <div class="modal fade" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledby="modal-label"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                    aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="modal-label"><spring:message code="savedmaps.save"/></h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" class="form-control"
                                   placeholder="<spring:message code="filter.enter.title"/>"
                                   id="filter-title">

                            <div id="color">
                                <select name="colorpicker">
                                    <option value="#7bd148">#7bd148</option>
                                    <option value="#5484ed">#5484ed</option>
                                    <option value="#a4bdfc">#a4bdfc</option>
                                    <option value="#46d6db">#46d6db</option>
                                    <option value="#7ae7bf">#7ae7bf</option>
                                    <option value="#51b749">#51b749</option>
                                    <option value="#fbd75b">#fbd75b</option>
                                    <option value="#ffb878">#ffb878</option>
                                    <option value="#ff887c">#ff887c</option>
                                    <option value="#dc2127">#dc2127</option>
                                    <option value="#dbadff">#dbadff</option>
                                    <option value="#e1e1e1">#e1e1e1</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message
                                    code="cancel"/></button>
                            <button id="save-filter" type="button" class="btn btn-primary" data-dismiss="modal">
                                <spring:message
                                        code="save"/></button>
                        </div>
                    </div>
                </div>
            </div>
            <%--End modal--%>


            <div class="row map-wrapper">
                <div class="map-container">
                    <div class="applied-filters">
                        <ul class="nav nav-pills ">
                            <li style="margin-left: 5px" class="active dropdown form-horizontal pull-right">
                                <a class="dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
                                    <spring:message code="maps.baselayer.list"/>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <%--<label class="label-map-provider">--%>
                                        <%--<input class="map-provider"--%>
                                        <%--value="mapQuest"--%>
                                        <%--name="provider"--%>
                                        <%--style='margin-right: 10px;margin-left: 5px' type='radio'>MapQuest</label>--%>
                                        <label class="label-map-provider">
                                            <input name="provider" value="openstreetmap" class="map-provider"
                                                   style='margin-right: 10px;margin-left: 5px'
                                                   type='radio'>Openstreetmap</label>
                                        <label class="label-map-provider">
                                            <input name="provider" value="esriGray" class="map-provider"
                                                   style='margin-right: 10px;margin-left: 5px'
                                                   type='radio'>ESRI Gray</label>
                                        <label class="label-map-provider">
                                            <input name="provider" value="esriTopo" class="map-provider"
                                                   style='margin-right: 10px;margin-left: 5px'
                                                   type='radio'>ESRI Topo</label>
                                    </li>
                                </ul>
                            </li>
                            <li class="active dropdown form-horizontal pull-right">
                                <a id="get-filters" class="btn-primary" href="#">
                                    <spring:message code="savedmaps.list"/>
                                </a>
                                <ul id="enabled-filters" class="dropdown-menu"></ul>
                            </li>
                            <li style="margin-right: 5px" class="active form-horizontal pull-right" data-toggle="modal"
                                data-target="#modal-dialog">
                                <a id="remember-map" class="btn-primary" href="#">
                                    <spring:message code="savedmaps.save"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="map" class="gis-map gis-map-square"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<content tag="javascript">
    <script>

        function setHeight(isClosed) {

            if ($(window).innerWidth() > 960) {

                $('.filters').css('height', 'auto');
                console.log('auto');

                var windowHeight = $(window).innerHeight();
                var offsetMap = $('#map').offset().top;
                var offsetFilters = $('.filters').offset().top;

                $('#map').css('height', windowHeight - offsetMap, +'px');

                if(typeof isClosed == 'undefined'){
                    isClosed = !$('#collapseFilters').hasClass('in')
                }

                if($('#collapseFilters').hasClass('in') || !isClosed) {
                    $('.filters').css('height', windowHeight - offsetFilters, +'px');
                    console.log('height');
                }
            }
            else {
                $(".filters").css('height', 'auto');
                $('#map').css('height', 600, +'px');
            }
        }
        $(document).ready(function() {
            setHeight();

            $(window).resize(function() {
                setHeight();
            });
            $(document).on('click', 'a[href*=#collapseFilters]', function() {
                $(this).toggleClass('closed');
                setHeight($(this).hasClass('closed'));
            });
        });

    </script>

    <script type="text/javascript">
        var jsonData = ${jsonFilter};
        localStorage.setItem("historyStep", 1);
        var mapProviders = getMapProviders();
        var cookieUtils = getCookieUtils();
        var map = L.map('map').setView([30, 0], 3);
        var layer = null;

        $(document).ready(function () {
            BrowseUtil.applySuggestions(jsonData, messages);

            $(document.getElementsByClassName("btn btn-default btn-back")).on('click', function (event) {
                event.preventDefault();
                var stepValue = localStorage.getItem("historyStep");
                window.history.go(-stepValue);
            });

            $('#collapseFilters').on('hidden.bs.collapse', function () {
                $("#content-area").addClass('fullwidth');
                map._onResize();
            }).children().on('hidden.bs.collapse', function () {
                return false;
            });

            $.each($('#collapseFilters').find("div.panel-collapse"), function () {
                <c:forEach items="${appliedFilters}" var="appliedFilter">
                var collapseDiv = $(this).find($("div[id*='${appliedFilter.key}']"));
                if (collapseDiv[0] !== undefined) {
                    $(this).collapse("show");
                }
                </c:forEach>
            });

            GenesysFilterUtil.registerAutocomplete(".filters", jsonData);

            $("body").on("keypress", ".string-type", function (e) {
                if (e.keyCode == 13) {
                    var btn = $(this).parent().find("button");
                    var selectedValue = $(this).parent().parent().find(".like-switcher option:selected").val();
                    if (selectedValue == "like") {
                        GenesysFilter.filterLike(btn, jsonData, BrowseUtil.i18nFilterMessage);
                    } else {
                        GenesysFilter.filterAutocomplete(btn, jsonData);
                    }
                }
            });

            $("body").on("click", ".filter-auto", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }

                var selectedValue = $(this).parent().parent().parent().find(".like-switcher option:selected").val();
                if (selectedValue == "like") {
                    GenesysFilter.filterLike($(this), jsonData, BrowseUtil.i18nFilterMessage);
                } else {
                    GenesysFilter.filterAutocomplete($(this), jsonData);
                }
            });

            $("body").on("click", ".filter-list", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                var text = $(this).parent().text();
                text = text.substring(0, text.indexOf('('));
                GenesysFilter.filterList($(this), jsonData, text);
            });

            $("body").on("click", ".filter-range", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterRange($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $("body").on("click", ".filter-bool", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterBoolean($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $('body').on('click', '.filter-crop', function () {
                GenesysFilter.filterCrop($(this), jsonData);
            });

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();
                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $("body").on("change", "#more-filters", function () {
                var key = $(this).val();
                var normKey = GenesysFilterUtil.normKey(key);
                var input = $(this).parent().find("input[type='text']");
                input.attr('id', normKey + "_input");
                if (key == "institute.country.iso3") {
                    input.attr('x-source', "/explore/ac/orgCty.iso3");
                } else {
                    input.attr('x-source', "/explore/ac/" + key);
                }

                var btn = $(this).parent().find("button.filter-auto");
                btn.attr("norm-key", normKey);
                btn.attr("i-key", key);
                jsonData[key] = [];
            });

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();
                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $('#remember-map').on('click', function(e) {
                e.preventDefault();
            });

            $("#menuShareLink").on("click", function () {
                if ($('#shortLink').val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '/explore/shorten-url',
                        data: {
                            'browser': "map", 'filter': JSON.stringify(jsonData)
                        },
                        success: function (response) {
                            var inp = $("#shortLink");
                            inp.val(response.shortUrl);
                            inp.select();
                        }
                    });
                } else {
                    console.log('No repeat.');
                }
            });

            $(window).on("popstate", function (e) {
                if (e.originalEvent.state !== null) {
                    jsonData = JSON.parse(decodeURIComponent((new RegExp('[?|&]filter=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20'))) || [];
                    page = decodeURIComponent((new RegExp('[?|&]page=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 1;
                    results = decodeURIComponent((new RegExp('[?|&]results=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 50;
                    var filter = JSON.stringify(jsonData);
                    var requestUrl = '//' + location.host + location.pathname +
                            "?filter=" + encodeURIComponent(filter);
                    $.ajax({
                        url: requestUrl,
                        method: 'get',
                        success: function (response) {
                            refreshData();
                        }
                    });
                    BrowseUtil.applySuggestions(jsonData, messages);
                } else {
                    location.reload();
                }
            });

            var topPane = map._createPane('leaflet-top-pane', map.getPanes().mapPane);

            var selectedBaseMap = cookieUtils.getCookie('mapProviders') || 'esriGray';
            if (mapProviders[selectedBaseMap] === undefined) {
                selectedBaseMap = 'esriGray';
            }
            [].forEach.call($('.label-map-provider input:radio'), function (radio) {
                $(radio).prop('checked', $(radio).val() === selectedBaseMap);
            });

            mapProviders[selectedBaseMap].addTo(map);
            topPane.appendChild(mapProviders[selectedBaseMap].getContainer());
            mapProviders[selectedBaseMap].setZIndex(0);

            loadTiles(map);

            $("body").on("click", ".applyBtn", function () {
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);
            });


            $(document).on('cookieUpdate', function () {
                for (var key in mapProviders) {
                    map.removeLayer(mapProviders[key]);
                }
                if (cookieUtils.getCookie('mapProviders')) {

                    mapProviders[cookieUtils.getCookie('mapProviders')].addTo(map);
                    topPane.appendChild(mapProviders[cookieUtils.getCookie('mapProviders')].getContainer());
                    mapProviders[cookieUtils.getCookie('mapProviders')].setZIndex(0);
                } else {

                    mapProviders.esriGray.addTo(map);
                    topPane.appendChild(mapProviders[cookieUtils.getCookie('mapProviders')].getContainer());
                    mapProviders.esriGray.setZIndex(0);
                }
            });

            $('.map-provider').click(function (e) {
                var providerName = $(this).val();
                if ($(this).is(":checked")) {
                    cookieUtils.setCookie('mapProviders', providerName, 0);
                    $(document).trigger('cookieUpdate');
                } else {
                    providerName = '';
                    cookieUtils.setCookie('mapProviders', providerName, 0);
                    $(document).trigger('cookieUpdate');
                }
            });

            $('#color select').simplecolorpicker();


            $("#save-filter").on("click", function (event) {
                event.preventDefault();

                var title = $("#filter-title").val();
                var filter = jsonData;

                $.ajax({
                    url: "/savedmaps/save",
                    type: "post",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        title: title,
                        filter: JSON.stringify(filter),
                        color: $('#color select').val()
                    }),
                    success: function (data) {

                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
            });

            $("#get-filters").on("click", function (event) {
                event.preventDefault();
                $.ajax({
                    url: "/savedmaps/get",
                    type: "get",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        $.each(data, function (idx, filter) {

                            var li = "<li><label  class='saved-filter' x-fil='" + filter.filter + "' x-color='" + filter.color + "'><input style='margin-right: 10px;margin-left: 5px' type='checkbox'>" + filter.title + "</label></li>";

                            if ($("#enabled-filters").is(":visible")) {
                                $("#enabled-filters").append(li);
                            } else {
                                $("#enabled-filters").empty();
                            }
                        });
                    },
                    error: function (error) {
                        console.log(error)
                    }
                });
                $("#enabled-filters").toggle();
            });

            $("#selectArea").hide();
            var filterJson =${jsonFilter};
            var filterJsonObj = {};
            if (typeof filterJson !== 'undefined') {
                filterJsonObj = JSON.parse(JSON.stringify(filterJson));
            }

            var layers = {};
            $("body").on("click", ".saved-filter", function (e) {
                var title = $(this).text();
                var filter = $(this).attr("x-fil");
                var tilesColor = $(this).attr("x-color").substring(1);
                var savedFilterObj = JSON.parse(filter);

                if ($(this).find("input:checkbox").is(":checked")) {
                    if (layers[title] == null) {
                        layers[title] = L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + filter + "&color=" + tilesColor, {
                            attribution: "<a href='${props.baseUrl}'>Genesys</a>",
                            styleId: 22677,
                            subdomains: [${props.tileserverCdn}]
                        }).addTo(map);
                    }
                } else {
                    if (layers[title] != null) {
                        map.removeLayer(layers[title]);
                        layers[title] = null;
                    }
                }
            });

            var locationFilter = new L.LocationFilter({
                adjustButton: false,
                bounds: map.getBounds().pad(-0.1)
            }).addTo(map);
            locationFilter.on("change", function (e) {
                // Do something when the bounds change.
                // Bounds are available in `e.bounds`.
                var bounds = locationFilter.getBounds();
                filterJson['geo.latitude'] = [{range: [niceDec(nicebounds.getSouth()), niceDec(bounds.getNorth())]}];
                filterJson['geo.longitude'] = [{range: [niceDec(bounds.getWest()), niceDec(bounds.getEast())]}];
            });

            map.on("viewreset", function () {
                if (locationFilter.isEnabled())
                    return;
                var mapBounds = map.getBounds().pad(-0.1);
                locationFilter.setBounds(mapBounds);
            });
            locationFilter.on("enabled", function () {
                // Do something when enabled.
                var bounds = locationFilter.getBounds();
                filterJson['geo.latitude'] = [{range: [niceDec(bounds.getSouth()), niceDec(bounds.getNorth())]}];
                filterJson['geo.longitude'] = [{range: [niceDec(bounds.getWest()), niceDec(bounds.getEast())]}];
                $("#selectArea").show();
            });

            locationFilter.on("disabled", function () {
                // Do something when disabled.
                $("#selectArea").hide();
            });

            $("#selectArea").on("click", function (e) {
                this.href = this.href + '?filter=' + JSON.stringify(filterJson);
            });

            var loadDetailsTimeout = null;
            var clickMarker = null;
            map.on("click", function (e) {
                if (clickMarker != null) {
                    map.removeLayer(clickMarker);
                    clickMarker = null;
                }
                if (map.getZoom() > 4) {
                    if (loadDetailsTimeout != null) {
                        clearTimeout(loadDetailsTimeout);
                    }

                    var point = this.latLngToLayerPoint(e.latlng);
                    point.x -= 5;
                    point.y += 5;
                    var sw = this.layerPointToLatLng(point);
                    point.x += 10;
                    point.y -= 10;
                    var ne = this.layerPointToLatLng(point);
                    loadDetailsTimeout = setTimeout(function () {

                        var filterBounds = filterJsonObj;
                        filterBounds['geo.latitude'] = [{range: [sw.lat, ne.lat]}];
                        filterBounds['geo.longitude'] = [{range: [sw.lng, ne.lng]}];
                        $.ajax("<c:url value="/explore/geoJson"><c:param name="limit" value="11" /></c:url>&filter=" + JSON.stringify(filterBounds), {
                            type: "GET",
                            dataType: 'json',
                            success: function (respObject) {
                                if (respObject.features == null || respObject.features.length == 0)
                                    return;

                                var c = "";
                                respObject.features.forEach(function (a, b) {
                                    if (b < 10)
                                        c += "<a href='<c:url value="/acn/id/" />" + a.id + "'>" + a.properties.acceNumb + " " + a.properties.instCode + "</a><br />";
                                    else
                                        c += "...";
                                });
                                clickMarker = L.rectangle([sw, ne], {stroke: false, fill: true}).addTo(map);
                                clickMarker.bindPopup(c).openPopup();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                                console.log(errorThrown);
                            }
                        });

                    }, 200);
                }
            });
            map.on("dblclick", function (e) {
                if (loadDetailsTimeout != null) {
                    //console.log("cleared" + loadDetailsTimeout);
                    clearTimeout(loadDetailsTimeout);
                    loadDetailsTimeout = null;
                }
            });
        });

        function getMapProviders() {
            return {
                openstreetmap: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                }),
                /*  OpenMapSurfer_Grayscale: L.tileLayer('http://openmapsurfer.uni-hd.de/tiles/roadsg/x={x}&y={y}&z={z}', {
                 maxZoom: 19,
                 attribution: 'Imagery from <a target="_blank" href="http://giscience.uni-hd.de/">GIScience Research Group, University of Heidelberg</a>, Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                 }),*/
                esriGray: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
                    attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
                    maxZoom: 16
                }),
                esriTopo: L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
                    attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
                })
            }
        }

        function getCookieUtils() {
            return {
                getCookie: function (name) {
                    var matches = document.cookie.match(new RegExp(
                            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                    ));
                    return matches ? decodeURIComponent(matches[1]) : undefined;
                },
                setCookie: function (name, value, options) {
                    options = options || {};

                    var expires = options.expires;

                    if (typeof expires == "number" && expires) {
                        var d = new Date();
                        d.setTime(d.getTime() + expires * 1000);
                        expires = options.expires = d;
                    }
                    if (expires && expires.toUTCString) {
                        options.expires = expires.toUTCString();
                    }

                    value = encodeURIComponent(value);

                    var updatedCookie = name + "=" + value;

                    for (var propName in options) {
                        updatedCookie += "; " + propName;
                        var propValue = options[propName];
                        if (propValue !== true) {
                            updatedCookie += "=" + propValue;
                        }
                    }
                    document.cookie = updatedCookie;
                },
                deleteCookie: function (name) {
                    this.setCookie(name, "", {
                        expires: -1
                    })
                }
            }
        }

        function niceDec(d, b) {
            if (b == null) b = 5;
            var x = Math.pow(10, b);
            return d === null ? null : Math.round(d * x) / x;
        }

        function loadTiles() {
            layer = L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + JSON.stringify(jsonData), {
                attribution: "Accession localities from <a href='${props.baseUrl}'>Genesys PGR</a>",
                styleId: 22677,
                subdomains: [${props.tileserverCdn}]
            });
            layer.addTo(map);
        }

        function reloadTiles() {
            if (layer !== null) {
                map.removeLayer(layer);
                layer = L.tileLayer("{s}/explore/tile/{z}/{x}/{y}?filter=" + JSON.stringify(jsonData), {
                    attribution: "Accession localities from <a href='${props.baseUrl}'>Genesys PGR</a>",
                    styleId: 22677,
                    subdomains: [${props.tileserverCdn}]
                });
                layer.addTo(map);
            }
            else {
                console.log("tileLayer is null");
            }
        }

        function refreshLinks() {
            var re = /filter=(.*)/;
            $("#exploreLink").attr("href", $("#exploreLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
            $("#overviewLink").attr("href", $("#overviewLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
        }

        function refreshData() {
            reloadTiles();
            refreshLinks();
        }

        function applyFilters() {
            var stepValue = (parseInt(localStorage.getItem("historyStep")) + 1);
            localStorage.setItem("historyStep", stepValue);

            BrowseUtil.cleanJsonData(jsonData);

            var filter = JSON.stringify(jsonData);
            var requestUrl = '//' + location.host + location.pathname +
                    "?filter=" + encodeURIComponent(filter);
            window.history.pushState(requestUrl, '', requestUrl);

            refreshData();
        }

    </script>
</content>
</body>
</html>