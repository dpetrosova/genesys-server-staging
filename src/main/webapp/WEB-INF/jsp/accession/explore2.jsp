<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>
<spring:message code='filter.internal.message.between' var="between" arguments=" "/>
<spring:message code='filter.internal.message.and' var="varEnd" arguments=" "/>
<spring:message code='filter.internal.message.more' var="moreThan" arguments=" "/>
<spring:message code='filter.internal.message.less' var="lessThan" arguments=" "/>
<spring:message code='filter.internal.message.like' var="like" arguments=" "/>
<spring:message code='boolean.true' var="varTrue"/>
<spring:message code='boolean.false' var="varFalse"/>
<spring:message code='boolean.null' var="varNull"/>

<html>
<head>
    <title><spring:message code="accession.page.data.title"/></title>
    <script type="text/javascript" src="<c:url value="/html/1/js/browse.js" />"></script>
    <script type="text/javascript" src="<c:url value="/explore/i18n.js"/>"></script>
</head>

<body class="explore-page">
<cms:informative-h1 title="accession.page.data.title" fancy="${pagedData.number eq 0}"
                    info="accession.page.data.intro"/>
<!-- Begin page content -->
    <div class="container-fluid">

        <div id="loading-popup-id" class="loading-popup">
            <div class="loading-popup-content">
                <p><spring:message code="prompt.loading-data" /></p>
            </div>
        </div>

        <div id="error-loading-popup-id" class="error-loading-popup">
            <div class="error-loading-popup-content">
                <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
                <p><spring:message code="prompt.loading-data-failed" /></p>
            </div>
        </div>

        <div id="dialog" class="row"></div>
        <div class="">
            <!--Filters-->
            <filters:filter-list availableFilters="${availableFilters}" filters="${filters}"
                                 appliedFilters="${appliedFilters}" crops="${crops}" crop="${crop}"/>
            <!--List-->
            <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 main-col-header">
                <div class="nav-header clearfix">
                    <!-- Links -->
                    <div class="pull-right list-view-controls">
                        <div class="btn-group" id="displayColumns">
                            <ul class="nav nav-pills">
                                <li class="dropdown form-horizontal" id="selectColumns">
                                    <a class="dropdown-toggle btn btn-default" href="#">
                                        <b class="glyphicon glyphicon-th-list"></b>
                                        <spring:message code="columns.add"/>
                                    </a>
                                    <ul class="dropdown-menu view-columns">
                                        <li>
                                            <a id="columnsApply" style="margin-bottom: 10px" type="submit"
                                               + class="btn btn-default"><spring:message code="columns.apply"/>
                                            </a>
                                        </li>
                                        <li class="dropdown-header">
                                            <spring:message code="columns.availableColumns"/>
                                        </li>
                                        <c:forEach items="${availableColumns}" var="columnName">
                                            <li>
                                                <label class="column-enable" for="cb-${columnName}">
                                                    <c:choose>
                                                        <c:when test="${selectedColumns.contains(columnName)}">
                                                            <input id="cb-${columnName}" type="checkbox"
                                                                   + checked="checked" name="${columnName}"/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <input id="cb-${columnName}" type="checkbox"
                                                                   + name="${columnName}"/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <spring:message code="accession.${columnName}"/>
                                                </label>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="btn-group" id="shareLink">
                            <a type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false" id="menuShareLink">
                                <span class="glyphicon glyphicon-share"></span>
                                <span><spring:message code="share.link"/></span>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="padding10">
                                    <p><spring:message code="share.link.text"/></p>
                                    <input id="shortLink" type="text"
                                           placeholder="<spring:message code="share.link.placeholder" />" value=""/>
                                </li>
                            </ul>
                        </div>

                        <a class="btn btn-default" id="overviewLink"
                           href="<c:url value="/explore/overview"><c:param name="filter">${jsonFilter}</c:param></c:url>">
                            <span class="glyphicon glyphicon-eye-open"></span>
                            <span><spring:message code="data-overview.short"/></span>
                        </a>
                        <a class="btn btn-default" id="mapLink"
                           href="<c:url value="/explore/map"><c:param name="filter">${jsonFilter}</c:param></c:url>"><span
                                class="glyphicon glyphicon-globe"></span>
                            <span><spring:message code="maps.view-map"/></span>
                        </a>
                    </div>
                    <!--Pagination-->
                    <filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
                </div>
            </div>
            <div id="content-area" class="col-lg-10 col-md-9 col-sm-9 col-xs-12 explore-table">
                <table class="accessions">
                    <thead>
                    <tr>
                        <td class="idx-col"><p></p></td>
                        <td><p></p></td>
                        <td><p><spring:message code="accession.accessionName" /></p></td>
                        <c:forEach items="${selectedColumns}" var="column" varStatus="i">
                            <c:choose>
                                <c:when test="${i.count eq 1}">
                                    <td><p><spring:message code="accession.${column}"/></p></td>
                                </c:when>
                                <c:when test="${i.count eq 2}">
                                    <td class="notimportant hidden-xs"><p><spring:message code="accession.${column}"/></p></td>
                                </c:when>
                                <c:when test="${column eq 'taxonomy.species'}">
                                    <td class="notimportant hidden-sm hidden-xs"><p><spring:message code="accession.${column}"/></p></td>
                                </c:when>
                                <c:otherwise>
                                    <td class="notimportant hidden-sm hidden-xs"><p><spring:message code="accession.${column}"/></p></td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${pagedData.content}" var="accession" varStatus="status">
                        <tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'} ${accession.historic ? 'historic-record' : ''}">
                            <td class="idx-col"><p>${status.count + pagedData.size * pagedData.number}</p></td>
                            <td><span class="sel" x-aid="${accession.id}" title="<spring:message code="selection.checkbox" />"></span></td>
                            <td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.acceNumb}"/></b></a></p></td>
                            <c:forEach items="${selectedColumns}" var="col" varStatus="i">
                                <c:choose>
                                    <c:when test="${col eq 'taxonomy.sciName'}">
                                        <td class="<c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.sciName}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'orgCty'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><c:out value="${jspHelper.getCountry(accession.orgCty.iso3).getName(pageContext.response.locale)}"/></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'cropName'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><c:out value="${accession.cropName}"/></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'sampStat'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><spring:message code="accession.sampleStatus.${accession.sampStat}"/></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'institute.code'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out
                                                value="${accession.institute.code}"/></a></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.genus'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.genus}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.species'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.species}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'taxonomy.subtaxa'}">
                                        <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><span dir="ltr" class="sci-name"><c:out value="${accession.taxonomy.subtaxa}"/></span></p></td>
                                    </c:when>
                                    <c:when test="${col eq 'crops'}">
                                        <c:if test="${fn:length(accession.crops) == 0}">
                                            <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"></td>
                                        </c:if>
                                        <c:forEach items="${jspHelper.getCrops(accession.crops)}" var="crop">
                                            <td class="notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"><p><a href="<c:url value="/c/${crop.shortName}" />"><c:out
                                                    value="${crop.getName(pageContext.response.locale)}"
                                                    /></a></p></td>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="elVal" value="${accession}" />
                                        <c:forTokens items="${col}" delims="." var="item">
                                            <c:set var="elVal" value="${elVal[item]}" />
                                        </c:forTokens>
                                        <td class="notimportant hidden-sm hidden-xs"><p><c:out value="${elVal}"/></p></td>
                                        <c:remove var="elVal"/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                    <div class="nav-header bottom-pagination-wrapper clearfix">
                        <!--Pagination-->
                        <filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
                        <nav class="text-center pull-right">
                            <ul class="pagination">
                                <li>
                                    <span><spring:message code="paged.resultsPerPage" /></span>
                                </li>
                                <li>
                                <span>
                                    <select class="form-control results-per-page">
                                        <option value="10" ${pagedData.size == 10 ? "selected" : ""}>10</option>
                                        <option value="20" ${pagedData.size == 20 ? "selected" : ""}>20</option>
                                        <option value="40" ${pagedData.size == 40 ? "selected" : ""}>40</option>
                                        <option value="50" ${pagedData.size == 50 ? "selected" : ""}>50</option>
                                        <option value="75" ${pagedData.size == 75 ? "selected" : ""}>75</option>
                                        <option value="100" ${pagedData.size == 100 ? "selected" : ""}>100</option>
                                    </select>
                                </span>
                                </li>
                            </ul>
                        </nav>
                    </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>

            <!--End of List-->
        </div>
    </div>


<content tag="javascript">
    <script type="text/javascript">
        var jsonData = ${jsonFilter};

        var page = ${pagedData.number};

        var results = ${pagedData.size};

        var cropNames = [];

        var headersCopy = $(".accessions").find("thead td").clone();

        var headerSelects = [];

        var rowsCopy = $(".accessions").find("tbody tr").clone();

        var lastWindowWidth;

        $(document).ready(function () {

            cropNames = function(){
                var data = null;
                $.ajax({
                    url: "/explore/getCrops",
                    method: 'get',
                    async: false
                }).done(function (response) {
                    data = response;
                });
                return data;
            }();

            $("body").on("keypress", ".string-type", function (e) {
                if (e.keyCode == 13) {
                    console.log("press")
                    var btn = $(this).parent().find("button");
                    var selectedValue = $(this).parent().parent().find(".like-switcher option:selected").val();

                    if (selectedValue == "like") {
                        GenesysFilter.filterLike(btn, jsonData, BrowseUtil.i18nFilterMessage);
                    } else {
                        console.log("btn", btn)
                        console.log("jsonData", jsonData)
                        console.log("03")
                        GenesysFilter.filterAutocomplete(btn, jsonData);
                        console.log("04")
                    }
                }
            });

            $("body").on("click", ".filter-auto", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }

                var selectedValue = $(this).parent().parent().parent().find(".like-switcher option:selected").val();
                if (selectedValue == "like") {
                    GenesysFilter.filterLike($(this), jsonData, BrowseUtil.i18nFilterMessage);
                } else {
                    GenesysFilter.filterAutocomplete($(this), jsonData);
                }
            });

            $("body").on("click", ".filter-list", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                var text = $(this).parent().text();
                text = text.substring(0, text.indexOf('('));
                GenesysFilter.filterList($(this), jsonData, text);
            });

            $("body").on("click", ".filter-range", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterRange($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $("body").on("click", ".filter-bool", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterBoolean($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $('body').on('click', '.filter-crop', function () {
                GenesysFilter.filterCrop($(this), jsonData);
            });

            $("body").on("click", ".applyBtn", function () {
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);
            });

            BrowseUtil.applySuggestions (jsonData, messages);

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();

                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $("#menuShareLink").on("click", function () {
                if ($('#shortLink').val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '/explore/shorten-url',
                        data: {
                            'page': 1, 'filter': JSON.stringify(jsonData), browser: "explorer"
                        },
                        success: function (response) {
                            var inp = $("#shortLink");
                            inp.val(response.shortUrl);
                            inp.select();
                        }
                    });
                } else {
                    console.log('No repeat.');
                }
            });

            $("body").on("change", "#more-filters", function() {
                var key = $(this).val();
                var normKey = GenesysFilterUtil.normKey(key);
                var input = $(this).parent().find("input[type='text']");
                input.attr('id', normKey + "_input");
                if (key == "institute.country.iso3") {
                    input.attr('x-source', "/explore/ac/orgCty.iso3");
                } else {
                    input.attr('x-source', "/explore/ac/" + key);
                }

                var btn = $(this).parent().find("button.filter-auto");
                btn.attr("norm-key", normKey);
                btn.attr("i-key", key);
                jsonData[key] = [];
            });

            $.each([$(".firstPage"), $(".previousPage"), $(".nextPage"), $(".lastPage")], function () {
                $(this).find("a").on("click", function (e) {
                    e.preventDefault();
                    applyFilters($(e.target).attr("href"));
                });
            });

            $("nav.text-center").find("form").on("submit", function (e) {
                e.preventDefault();
                applyFilters("?" + $(this).serialize());
            });

            $(".results-per-page").on("change", function () {
                results = $(this).val();
                applyFilters();
            });

            $.each($('#collapseFilters').find("div.panel-collapse"), function () {
                <c:forEach items="${appliedFilters}" var="appliedFilter">
                    var collapseDiv = $(this).find($("div[id*='${appliedFilter.key}']"));
                    if (collapseDiv[0] !== undefined) {
                        $(this).collapse("show");
                    }
                </c:forEach>
            });

            $('li#selectColumns a').on('click', function (event) {
                event.preventDefault();
                $(this).parent().toggleClass('open');
            });

            $('#columnsApply').on('click', function () {
                var columns = [];
                [].forEach.call($('.view-columns li'), function (el) {
                    if ($(el).find('input:checkbox').is(':checked')) {
                        columns.push($(el).find('input:checkbox').prop('name'));
                    }
                });
                var filter = JSON.stringify(jsonData);
                var url = '<c:url value="/explore" />' + '?filter=' + filter + "&columns=" + columns.toString();
                window.location.href = encodeURI(url);
            });

            $(window).on("popstate", function(e) {
                if (e.originalEvent.state !== null) {
                    jsonData = JSON.parse(decodeURIComponent((new RegExp('[?|&]filter=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20'))) || [];
                    page = decodeURIComponent((new RegExp('[?|&]page=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 1;
                    results = decodeURIComponent((new RegExp('[?|&]results=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 50;
                    var filter = JSON.stringify(jsonData);
                    var requestUrl = '//' + location.host + location.pathname + "/json?page=" + page + "&filter=" + filter + "&results=" + results;
                    $.ajax({
                        url: requestUrl,
                        method: 'get',
                        success: function (response) {
                            renderData(response);
                        }
                    });
                    BrowseUtil.applySuggestions(jsonData, messages);
                } else {
                    location.reload();
                }
            });

            GenesysFilterUtil.registerAutocomplete(".filters", jsonData);

            $.ajax({
                url: "/explore/booleanSuggestions",
                method: 'get',
                data: {
                    filter: JSON.stringify(jsonData)
                },
                success: function (response) {
                    BrowseUtil.renderBooleanSuggestions(response);
                }
            });

            function restoreHeaders() {
                $.each($("select.column-changer"), function () {
                    var index = $(this).attr("x-id");
                    var oldHeader = $(headersCopy[index]).html();
                    $(this).parent().html(oldHeader);
                });
            }

            function resize() {
                if (window.innerWidth < 992) {
                    var headers = $(".accessions").find("thead td");
                    $.each(headersCopy, function (index) {
                        if (index >= 3 && headerSelects[index] != true) {
                            var select = $("<select/>", {
                                'class': 'column-changer',
                                'x-id': index
                            });

                            var selectedOption = $("<option/>", {
                                value: $(this).text(),
                                text: $(this).text(),
                                'x-id': index,
                                selected: true
                            });
                            selectedOption.appendTo(select);

                            for (var i = 3; i < headers.length; i++) {
                                if ($(headers[i]).text() !== $(headers[index]).text()) {
                                    var option = $("<option/>", {
                                        value: $(headersCopy[i]).text(),
                                        text: $(headersCopy[i]).text(),
                                        'x-id': i
                                    });

                                    option.appendTo(select);
                                }
                            }

                            $(headers[index]).html(select);
                            select.after('<span class="glyphicon glyphicon-menu-down"></span>');
                            headerSelects[index] = true;
                        }
                    });

                    $.each($("select.column-changer"), function () {
                        $(this).on("change", function() {
                            var cols;
                            var from = $(this).find("option:selected").attr("x-id");
                            var to = $(this).attr("x-id");
                            $.each($(".accessions").find("tbody tr"), function(index) {
                                cols = $(this).children('td');
                                var val = $(rowsCopy[index]).children().eq(from).html();
                                cols.eq(to).html(val);
                            });
                        });
                    });
                } else {
                    restoreHeaders();
                    headerSelects = [];
                    if (lastWindowWidth < 992) {
                        var path = "?page=" + (page + 1) + "&" + "filter=" + encodeURIComponent(JSON.stringify(jsonData));
                        applyFilters(path);
                    }
                }

                lastWindowWidth = window.innerWidth;
            }

            resize();

            $(window).resize(function () {
                resize();
            });
        });

        function applyFilters (path) {
            document.getElementById('loading-popup-id').style.display = "block";
            BrowseUtil.cleanJsonData(jsonData);
            var filter = JSON.stringify(jsonData);
            console.log("filter: " , filter);
            var requestUrl = '//' + location.host + location.pathname + "/json" +
                    (path !== undefined ? path : "?page=1&" + "filter=" + encodeURIComponent(filter));
            requestUrl += "&results=" + results;
            var displayUrl = requestUrl.replace('/json', '');

            window.history.pushState(requestUrl, '', displayUrl);
            $.ajax({
                url: requestUrl,
                method: 'get',
                success: function (response) {
                    renderData(response);
                    document.getElementById('loading-popup-id').style.display = "none";
//                        document.getElementById('error-loading-popup-id').style.display = "block";
                    $.each($("select.column-changer"), function () {
                        $(this).change();
                    });
                },
                error: function () {
                    document.getElementById('error-loading-popup-id').style.display = "block";
                }
            });
        }

        function renderData(pagedData) {
            page = pagedData.number;
            renderPagination(pagedData);
            renderLinks(pagedData);
            renderTableBody(pagedData);
        }

        function renderPagination (pagedData) {
            var re = /\B(?=(\d{3})+(?!\d))/g;
            var totalElements = pagedData.totalElements.toString().replace(re, ",");
            var totalPages = pagedData.totalPages.toString().replace(re, ",");
            $('nav').find(".results").html(messages["accessions.number"].replace("{0}", totalElements));
            $('.pagination').find(".totalPages").html(messages["paged.ofPages"].replace("{0}", totalPages));
            setPageUrl($(".firstPage"), 1);
            setPageUrl($(".previousPage"), page == 0 ? 1 : page);
            setPageUrl($(".nextPage"), page + 2);
            setPageUrl($(".lastPage"), pagedData.totalPages);
            $(".pagination-input").find("input").val(page + 1);
            $("input[name='filter']").val(JSON.stringify(jsonData));
        }

        function setPageUrl (element, page) {
            $(element).find("a").attr("href", "?page=" + page + "&filter=" + encodeURI(JSON.stringify(jsonData)));
        }

        function renderLinks (pagedData) {
            var downloadLink = $("#downloadLink");
            if (pagedData.totalElements <= 200000) {
                downloadLink.show();
                $.each($("#downloadLink").find("input[name='filter']"), function () {
                    $(this).val(JSON.stringify(jsonData));
                });
            } else {
                downloadLink.hide();
            }
            var re = /filter=(.*)/;
            $("#overviewLink").attr("href", $("#overviewLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
            $("#mapLink").attr("href", $("#mapLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
        }

        function renderTableBody(pagedData) {
            console.log(pagedData);
            $("table").find("tbody").remove();
            var tbody = $("<tbody/>");
            rowsCopy = [];
            for (var i = 0; i < pagedData.numberOfElements; i++) {
                var row = $("<tr/>", {
                    'class': "acn " + (i % 2 == 0 ? 'odd' : 'even') + (pagedData.content[i].historic ? ' historic-record' : '')
                });
                $("<p/>", {
                    text: (i + 1) + pagedData.size * pagedData.number
                }).appendTo($("<td/>", {
                    'class': "idx-col"
                }).appendTo(row));
                $("<span/>", {
                    'class': "sel",
                    'x-aid': pagedData.content[i].id,
                    'title': "<spring:message code="selection.checkbox" />"
                }).appendTo($("<td/>").appendTo(row));
                $("<b/>", {
                    text: pagedData.content[i].acceNumb
                }).appendTo(
                $("<a/>", {
                    href: '<c:url value="/acn/id/" />' + pagedData.content[i].id
                }).appendTo($("<p/>").appendTo($("<td/>").appendTo(row))));

                <c:forEach items="${selectedColumns}" var="col" varStatus="i">
                    <c:choose>
                        <c:when test="${col eq 'taxonomy.sciName'}">
                            $("<span/>", {
                                dir: 'ltr',
                                'class': 'sci-name',
                                text: pagedData.content[i].taxonomy.sciName
                            }).appendTo($("<p></p>").appendTo($("<td class='<c:if test="${i.count ge 2}">hidden-xs</c:if> <c:if test="${i.count gt 2}">hidden-sm</c:if>'></td>").appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'orgCty'}">
                            $("<p/>", {
                                text: messages['geo.iso3166-3.' + pagedData.content[i].orgCty.iso3]
                            }).appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row));
                        </c:when>
                        <c:when test="${col eq 'sampStat'}">
                            $("<p/>", {
                                text: messages["accession.sampleStatus." + pagedData.content[i].sampStat]
                            }).appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row));
                        </c:when>
                        <c:when test="${col eq 'institute.code'}">
                            $("<a/>", {
                                href: '<c:url value="/wiews/" />' + pagedData.content[i].institute.code,
                                text: pagedData.content[i].institute.code
                            }).appendTo($("<p></p>").appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.genus'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.genus
                            }).appendTo($("<p></p>").appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.species'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.species
                            }).appendTo($("<p></p>").appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'taxonomy.subtaxa'}">
                            $("<span/>", {
                                dir: "ltr",
                                'class': "sci-name",
                                text: pagedData.content[i].taxonomy.subtaxa
                            }).appendTo($("<p></p>").appendTo($("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'crops'}">
                            var cropNamesTd = $("<td/>", {
                                'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                            });
                            if (pagedData.content[i].crops != null) {
                                $.each(pagedData.content[i].crops, function (index, shortName) {
                                    $("<a/>", {
                                        href: '<c:url value="/c/" />' + shortName,
                                        text: cropNames[shortName]
                                    }).appendTo(cropNamesTd);
                                });
                            }
                            cropNamesTd.appendTo(row);
                        </c:when>
                        <c:when test="${col eq 'cropName'}">
                        $("<span/>", {
                            dir: "ltr",
                            'class': "sci-name",
                            text: pagedData.content[i].cropName
                        }).appendTo($("<p></p>").appendTo($("<td/>", {
                            'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                        }).appendTo(row)));
                    	</c:when>
                        <c:when test="${col eq 'geo.latitudeAndLongitude'}">
                        var geoLatitude = "";
                        var geoLongitude = "";
                        if (pagedData.content[i].geo != null) {
                            geoLatitude = pagedData.content[i].geo.latitude;
                            geoLongitude = pagedData.content[i].geo.longitude;
                        }
                        var geoText = "";
                        if (geoLatitude !== null && geoLatitude.toString() != "") {
                            geoText = geoLatitude + ", " + geoLongitude
                        }
                        $("<span/>", {
                            dir: "ltr",
                            'class': "sci-name",
                            text: geoText
                        }).appendTo($("<p></p>").appendTo($("<td/>", {
                            'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                        }).appendTo(row)));
                        </c:when>
                        <c:when test="${col eq 'coll.collMissId'}">
                        var collMissIdText = "";
                        if (pagedData.content[i].coll != null) {
                            if (pagedData.content[i].coll.collMissId != null) {
                                collMissIdText = pagedData.content[i].coll.collMissId;
                            }
                        }
                        $("<span/>", {
                            dir: "ltr",
                            'class': "sci-name",
                            text: collMissIdText
                        }).appendTo($("<p></p>").appendTo($("<td/>", {
                            'class': "notimportant <c:if test='${i.count ge 2}'>hidden-xs</c:if> <c:if test='${i.count gt 2}'>hidden-sm</c:if>"
                        }).appendTo(row)));
                        </c:when>
                    </c:choose>
                </c:forEach>

                row.appendTo(tbody);
                rowsCopy.push($(row).clone());
            }

            tbody.appendTo($("table"));
        }

    </script>
</content>
</body>
</html>

          