<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="content.page.list.title" /></title>
</head>
<body class="article-page selected-list">
	<h1 class="green-bg">
		<spring:message code="content.page.list.title" />
	</h1>

	<div class="row main-col-header clearfix">
	<div class="nav-header">
		<%--<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}" /></div>--%>
		<%--<div class="pagination pull-left">
			<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
			<a class="${pagedData.number eq 0 ? 'disabled' :''}" href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}&amp;language=${language}"><spring:message code="pagination.previous-page" /></a> <a href="?page=${pagedData.number + 2}&amp;language=${language}"><spring:message code="pagination.next-page" /></a>
		</div>--%>
        <div class="form-group pull-right">
            <div class="dropdown">
                <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><c:out value="Languge filtering" /> <b class="caret"></b></a>
                <ul  class="dropdown-menu" role="menu" id="transifiex_langs">
                    <li><a href="${pageContext.request.contextPath}/content">All</a></li>
                    <c:forEach items="${languages}" var="language">
                        <li><a href="?language=${language.value}"><c:out value="${language.key}" /></a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
			<local:paginate2 page="${pagedData}"/>
	</div>
	</div>
	
	<table class="accessions">
		<thead>
			<tr>
				<td class="idx-col"><p></p></td>
				<td><p></p></td>
				<td><p><spring:message code="article.slug" /></p></td>
				<td><p><spring:message code="article.lang" /></p></td>
				<td><p><spring:message code="article.classPk" /></p></td>
				<td><p><spring:message code="article.title" /></p></td>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${pagedData.content}" var="article" varStatus="status">
			<tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
				<td class="idx-col"><p><c:out value="${status.count + pagedData.size * pagedData.number}" /></p></td>
				<td class="sel ${selection.containsId(accession.id) ? 'picked' : ''}" x-aid="${accession.id}"></td>
				<td>
					<c:choose>
						<c:when test="${article.targetId ne null}">
							<a href="<c:url value="/content/edit/${article.slug}/${article.classPk.shortName}/${article.targetId}/${article.lang}" />"><c:out value="${article.slug}" /></a>
						</c:when>
						<c:otherwise>
							<a href="<c:url value="/content/edit/${article.slug}/${article.lang}" />"><c:out value="${article.slug}" /></a>
						</c:otherwise>
					</c:choose>
				</td>
				<td><c:out value="${article.lang}" /></td>
				<td><c:out value="${article.classPk.shortName} ${article.targetId}" /></td>
				<td><c:out value="${article.title}" /></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>


</body>
</html>