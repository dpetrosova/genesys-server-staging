<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
	<title><spring:message code="news.content.page.all.title"/></title>
	<link href="<c:url value="/content/news/feed.atom" />" type="application/atom+xml" rel="alternate" title="Genesys News ATOM Feed" />
</head>
<body class="selected-list news-page">
	<div class="main-col clearfix">
		<%--<cms:informative-h1 title="news.content.page.all.title" fancy="true" />--%>

		<h1 class="green-bg"><spring:message code="news.content.page.all.title" /></h1>

		<div class="row main-col-header clearfix">
	        <div class="nav-header clearfix">
	            <%--<div class="results"><spring:message code="paged.totalElements" arguments="${pagedData.totalElements}"/></div>--%>
					<%--<div class="pagination pull-left">
						<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}"/>
						<a class="${pagedData.number eq 0 ? 'disabled' :''}"
						   href="?page=${pagedData.number eq 0 ? 1 : pagedData.number}"><spring:message
								code="pagination.previous-page"/></a> <a href="?page=${pagedData.number + 2}"><spring:message
							code="pagination.next-page"/></a>
					</div>--%>

					<!--Pagination-->
					<local:paginate2 page="${pagedData}"/>
	        </div>
		</div>

		<div class="row institute-list" dir="ltr">
			<c:forEach items="${pagedData.content}" var="news" varStatus="status">
				<div class="col-sm-12 clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<a class="pull-left" href="<c:url value="/content/news/${news.id}/${jspHelper.suggestUrlForText(news.title)}" />"><c:out value="${jspHelper.htmlToText(news.title)}" /></a>
					<div class="pull-right"><local:prettyTime date="${news.postDate.time}" locale="${pageContext.response.locale}" /></div>
				</div>
			</c:forEach>
		</div>

	</div>
</body>
</html>



