<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><c:out value="${jspHelper.htmlToText(news.title)}" /></title>
</head>
<body class="about-page text-page news-item-page">
<content tag="header">
    <div class="hidden-xs top-image">
        <img src="<c:url value="/html/0/images/aim/banner-${jspHelper.randomInt(1, 4)}.jpg" />" title="${title}" />
    </div>
    <div class="top-title">
            <div class="container" style="position: absolute; right: 0; padding: 10px 0;">
                <security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')">
                    <form class="inline-form pull-right" >
                         <a href="<c:url value="/content/activitypost/${news.id}/edit"/>" class="btn btn-default">
                            <spring:message code="edit" />
                        </a>
                        <a href="<c:url value="/content/activitypost/new"/>" class="btn btn-default">
                            <spring:message code="activitypost.add-new-post" />
                        </a>
                    </form>
                </security:authorize>
            </div>
    </div>
</content>

<div class="row">
    <h1 class="green-bg"><c:out value="${news.title}" escapeXml="false" /></h1>
        <div class="article clearfix">
            <div class="right-side main-col col-md-9">

                <div class="free-text" dir="ltr">
                    <c:out value="${news.body}" escapeXml="false" />
                </div>

                <div class="article-timestamp">
                    <local:prettyTime date="${news.postDate.time}" locale="${pageContext.response.locale}" />
                </div>

                <div class="share-article">
                    <p><spring:message code="article.share" /></p>
                    <ul class="list-inline">
                        <li class="twitter"><local:tweet text="${news.title}" hashTags="GenesysPGR" /></li>
                        <li class="linkedin"><local:linkedin-share text="${news.title}" summary="${news.body}" /></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 sidebar-nav col-xs-12" dir="ltr">
                <cms:menu key="${menu.key}" items="${menu.items}" />
            </div>
        </div>
    </div>
</body>
</html>



