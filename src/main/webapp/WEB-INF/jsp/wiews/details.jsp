<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><c:out value="${faoInstitute.fullName}" /><c:if test="${faoInstitute.acronym != ''}"> | <c:out value="${faoInstitute.acronym}" /></c:if> |
	<c:out value="${faoInstitute.code}" /></title>

<local:content-headers description="${jspHelper.htmlToText(blurp.summary, 150)}" title="${faoInstitute.fullName}" keywords="${faoInstitute.acronym}, ${faoInstitute.fullName}, ${faoInstitute.code}" />

<meta name="description"
	content="<spring:message code="faoInstitute.meta-description" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}" argumentSeparator="|" />
  	<c:out value="${jspHelper.htmlToText(blurp.summary)}" />" />
</head>
<body typeof="schema:Organization" class="wiews-institute">
	<h1 class="green-bg">
		<%--<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}/${faoInstitute.country.code3.toUpperCase()}.svg" />" />--%>
		<c:if test="${faoInstitute.acronym != ''}"><c:out value="${faoInstitute.acronym}" /></c:if>
		<small><c:out value="${faoInstitute.code}" /></small>
	</h1>

<%-- <div class="col-sm-4">
	<spring:message code="faoInstitute.code" />:
	<c:out value="${faoInstitute.code}" />
</div>
<div class="col-sm-4">
	<spring:message code="faoInstitute.acronym" />:
	<c:out value="${faoInstitute.acronym}" />
</div> --%>
	<%-- <div class="col-sm-4">
	<spring:message code="faoInstitute.email" />:
	<c:out value="${faoInstitute.email}" />
</div> --%>
	<%-- 		<p>
		<c:out value="${faoInstitute.type}" />
	</p>
--%>

<div class="row map-wrapper dark-background">
	<div class="genebank-info-wrapper clearfix">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 genebank-info striped">
				<h1 property="schema:Organization#name"><c:out value="${faoInstitute.fullName}" /></h1>
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="faoInstitute.country" /></p></div>
					<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8 text-ellipsis" property="schema:Organization#location">
						<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
							<a href="<c:url value="/geo" />" itemprop="url"><span itemprop="title"><spring:message code="menu.countries" /></span></a>
							<span class="glyphicon glyphicon-menu-right"></span>
							<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
								<a href="<c:url value="/geo/${faoInstitute.country.code3}" />" itemprop="url">
									<span itemprop="title"><c:out value="${faoInstitute.country.getName(pageContext.response.locale)}" /></span>
								</a>
							</span>
						</span>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="faoInstitute.url" /></p></div>
					<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8 text-ellipsis">
						<c:forEach items="${faoInstitute.safeUrls}" var="url">
							<div><a target="_blank" rel="nofollow" href="<c:out value="${url}" />">
								<span dir="ltr"><c:out value="${url}" /></span>
							</a></div>
						</c:forEach>
						<c:choose>
							<c:when test="${'en es fr ru ar zh'.indexOf(pageContext.response.locale.language) ge 0}"><c:set var="wiewsLang" value="${pageContext.response.locale.language}" /></c:when>
							<c:otherwise><c:set var="wiewsLang" value="en" /></c:otherwise>
						</c:choose>
						<div><a target="_blank" rel="nofollow"
							href="<c:url value="http://www.fao.org/wiews/instab/${wiewsLang}/"><c:param name="instcode" value="${faoInstitute.code}" /></c:url>">
							<spring:message code="faoInstitute.wiewsLink" arguments="${faoInstitute.code}" />
						</a></div>
					</div>
				</div>

				<c:if test="${organizations.size() gt 0}">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="faoInstitute.member-of-organizations-and-networks" /></p></div>
						<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8">
							<c:forEach items="${organizations}" var="organization">
								<a href="<c:url value="/org/${organization.slug}" />">
									<span dir="ltr"><c:out value="${organization.title}" /></span>
								</a>
							</c:forEach>
						</div>
					</div>
				</c:if>

				<div class="row accessionCount">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="faoInstitutes.stat.accessionCount" /></p></div>
					<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8">
						<p><span class="stats-number"><fmt:formatNumber value="${countByInstitute}" /></span></p>
						<div class="buttons-container pull-right">
							<c:if test="${countByInstitute gt 0}">
							<a class="btn btn-primary" title="<spring:message code="faoInstitute.data-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/data" />">
								<span class="glyphicon glyphicon-list"></span>
								<spring:message code="view.accessions" />
							</a>
							<a class="btn btn-default" title="<spring:message code="faoInstitute.overview-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/overview" />">
								<span class="glyphicon glyphicon-search"></span>
								<spring:message code="data-overview.short" />
							</a>
							<c:if test="${datasetCount gt 0}">
								<a class="btn btn-default" title="<spring:message code="faoInstitute.datasets-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/datasets" />">
									<spring:message code="view.datasets" />
								</a>
							</c:if>
							<div class="btn-group">
								<form class="form-horizontal" style="display:inline;" method="post" action="<c:url value="/download/wiews/${faoInstitute.code}/download" />">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="glyphicon glyphicon-save"></span>
										<span>
											<spring:message code="download" />
										</span>
										<span class="caret"></span>
									</button>

									<ul class="dropdown-menu">
										<security:authorize access="isAuthenticated()">
											<li>
												<button name="mcpd" class="btn btn-inline" type="submit">
													<spring:message code="filter.download-mcpd" />
												</button>
												<button name="pdci" class="btn btn-inline" type="submit">
													<spring:message code="filter.download-pdci" />
												</button>
											</li>
										</security:authorize>
										<li>
											<button name="dwca" class="btn btn-inline" type="submit">
												<spring:message code="metadata.download-dwca" />
											</button>
										</li>
									</ul>
								</form>
							</div>
						</c:if>
					</div>
				</div>
				</div>

				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4"><p><spring:message code="faoInstitutes.stat.datasetCount" /></p></div>
					<div class="col-lg-9 col-md-9 col-sm-6 col-xs-8"><p><span class="stats-number"><fmt:formatNumber value="${datasetCount}" /></span></p>
					</div>
				</div>

				<div class="row buttons-wrapper">
					<c:if test="${countByInstitute gt 0}">
						<a class="btn btn-primary" title="<spring:message code="faoInstitute.data-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/data" />">
							<span class="glyphicon glyphicon-list"></span>
							<spring:message code="view.accessions" />
						</a>
						<a class="btn btn-default" title="<spring:message code="faoInstitute.overview-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/overview" />">
							<span class="glyphicon glyphicon-search"></span>
							<spring:message code="data-overview.short" />
						</a>
						<c:if test="${datasetCount gt 0}">
							<a class="btn btn-default" title="<spring:message code="faoInstitute.datasets-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/datasets" />">
								<spring:message code="view.datasets" />
							</a>
						</c:if>
						<div class="btn-group">
							<form class="form-horizontal" style="display:inline;" method="post" action="<c:url value="/download/wiews/${faoInstitute.code}/download" />">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="glyphicon glyphicon-save"></span>
										<span>
											<spring:message code="download" />
										</span>
									<span class="caret"></span>
								</button>

								<ul class="dropdown-menu">
									<security:authorize access="isAuthenticated()">
										<li>
											<button name="mcpd" class="btn btn-inline" type="submit">
												<spring:message code="filter.download-mcpd" />
											</button>
											<button name="pdci" class="btn btn-inline" type="submit">
												<spring:message code="filter.download-pdci" />
											</button>
										</li>
									</security:authorize>
									<li>
										<button name="dwca" class="btn btn-inline" type="submit">
											<spring:message code="metadata.download-dwca" />
										</button>
									</li>
								</ul>
							</form>
						</div>
					</c:if>
				</div>
				<%-- <c:if test="${faoInstitute.latitude ne null}">
					<div class="row">
						<div class="col-xs-3"></div>

						<div class="col-xs-9">
							<span property="schema:Organization#location">
								<span typeof="schema:Place">
									<span property="schema:Place#geo">
										<span typeof="schema:GeoCoordinates">
											<span property="schema:GeoCoordinates#latitude"><c:out value="${faoInstitute.latitude}" /></span>
											,
											<span property="schema:GeoCoordinates#longitude"><c:out value="${faoInstitute.longitude}" /></span>
										</span>
									</span>
								</span>
							</span>
						</div>
					</div>
				</c:if> --%>

				<%--<div class="row">
					<div class="col-xs-12">
						<local:tweet text="${faoInstitute.fullName}" hashTags="${faoInstitute.code},${faoInstitute.acronym},GenesysPGR" iconOnly="true" />
						<local:linkedin-share text="${faoInstitute.fullName}" summary="${blurp.body}" iconOnly="true" />
					</div>
				</div>--%>
		</div>
		<c:if test="${faoInstitute.latitude ne null}">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 map-container" style="">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div id="map" class="gis-map"></div>
				</div>
			</div>
		</c:if>
	</div>
</div>

	<gui:alert type="warning" display="${not faoInstitute.current}">
		<spring:message code="faoInstitute.institute-not-current" />
		<a href="<c:url value="/wiews/${faoInstitute.vCode}" />"> <spring:message code="faoInstitute.view-current-institute" arguments="${faoInstitute.vCode}" />
		</a>
	</gui:alert>

	<gui:alert type="info" display="${faoInstitute.current and countByInstitute eq 0}">
		<spring:message code="faoInstitute.no-accessions-registered" />
	</gui:alert>

		<%--<h3>
			<span>
				<spring:message code="heading.general-info" />
			</span>
		</h3>
		<div class="row">
			<div class="col-md-offset-2 col-md-10">
				<ul class="funny-list statistics">
					<li class="clearfix odd">
						<span class="stats-number">
							<fmt:formatNumber value="${countByInstitute}" />
						</span>
						<spring:message code="faoInstitutes.stat.accessionCount" />
					</li>
					<li class="clearfix even">
						<span class="stats-number">
							<fmt:formatNumber value="${datasetCount}" />
						</span>
						<spring:message code="faoInstitutes.stat.datasetCount" />
					</li>
				</ul>
				<c:if test="${datasetCount gt 0}">
					<p><spring:message code="statistics.phenotypic.stats-text" arguments="${statisticsPheno.elStats}" /></p>
				</c:if>

				<c:if test="${countByInstitute gt 0}">
						<div class="row" style="margin-top: 2em;">
							<div class="col-sm-12">
								<a class="btn btn-primary" title="<spring:message code="faoInstitute.data-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/data" />">
										<span class="glyphicon glyphicon-list"></span>
									<spring:message code="view.accessions" />
								</a>
								<a class="btn btn-default" title="<spring:message code="faoInstitute.overview-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/overview" />">
									<span class="glyphicon glyphicon-eye-open"></span>
									<spring:message code="data-overview.short" />
								</a>
								<c:if test="${datasetCount gt 0}">
									<a class="btn btn-default" title="<spring:message code="faoInstitute.datasets-title" arguments="${faoInstitute.fullName}" />" href="<c:url value="/wiews/${faoInstitute.code}/datasets" />">
										<spring:message code="view.datasets" />
									</a>
								</c:if>
								<div class="btn-group">
								<form class="form-horizontal" method="post" action="<c:url value="/download/wiews/${faoInstitute.code}/download" />">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span class="glyphicon glyphicon-download"></span>
										<span>
											<spring:message code="download" />
										</span>
										<span class="caret"></span>
									</button>

									<ul class="dropdown-menu">
										<security:authorize access="isAuthenticated()">
											<li>
												<button name="mcpd" class="btn btn-inline" type="submit">
													<spring:message code="filter.download-mcpd" />
												</button>
												<button name="pdci" class="btn btn-inline" type="submit">
													<spring:message code="filter.download-pdci" />
												</button>
											</li>
										</security:authorize>
										<li>
											<button name="dwca" class="btn btn-inline" type="submit">
												<spring:message code="metadata.download-dwca" />
											</button>
										</li>
									</ul>
								</form>
							</div>
							</div>
						</div>
				</c:if>
			</div>
		</div>--%>

<div class="statistics-wrapper clearfix">
		<c:if test="${statisticsCrops ne null and statisticsCrops.totalCount gt 0}">
			<div class="collect-info col-lg-6 col-md-12 col-sm-12 col-xs-12 stat-by-crop">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-crop" /></span></h4>
				<c:set var="rowCount" value="${statisticsCrops.terms.size()}" />
					<c:if test="${rowCount gt 4}">
						<c:set var="cssClass" value="min-height"></c:set>
					</c:if>
				<div class="section-inner-content clearfix ${cssClass}">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 statistics">
						<local:term-result termResult="${statisticsCrops}" type="crop" />
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${statisticsCropName ne null and statisticsCropName.numberOfElements gt 0}">
			<div class="collect-info col-lg-6 col-md-12 col-sm-12 col-xs-12 stat-by-genus">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-cropName" arguments="${statisticsGenus.numberOfElements}" /></span></h4>
				<div class="section-inner-content clearfix ${cssClass}">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statistics">
						<c:forEach items="${statisticsCropName.content}" var="stat" varStatus="status">
							<div class="row">
								<div class="col-lg-8 col-md-6 col-sm-6 col-xs-6 ${status.count % 2 == 0 ? 'even' : 'odd'}">
									<c:choose>
										<c:when test="${stat[0] eq null}">
											<span>
												<em>
													<spring:message code="data-overview.missingCount" />
												</em>
											</span>
										</c:when>
										<c:otherwise>
											<a title="<spring:message code="faoInstitute.link-species-data" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}|${stat[0]}" argumentSeparator="|" />"
												href="<c:url value="/wiews/${faoInstitute.code}/cropName/${stat[0]}" />">
												<span dir="ltr">
													<c:out value="${stat[0]}" />
												</span>
											</a>
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 ${status.count % 2 == 0 ? 'even' : 'odd'}">
									<span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="chart chart-pie">
							<div id="chartStatsByCropName" style="height: 192px;"></div>
						</div>
					</div>
				</div>
			</div>
		</c:if>


		<c:if test="${statisticsGenus ne null and statisticsGenus.numberOfElements gt 0}">
			<div class="collect-info col-lg-6 col-md-12 col-sm-12 col-xs-12 stat-by-genus">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-genus" arguments="${statisticsGenus.numberOfElements}" /></span></h4>
				<div class="section-inner-content clearfix ${cssClass}">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statistics">
						<c:forEach items="${statisticsGenus.content}" var="stat" varStatus="status">
							<div class="row">
								<div class="col-lg-8 col-md-6 col-sm-6 col-xs-6 ${status.count % 2 == 0 ? 'even' : 'odd'}">
									<%-- <span class="index-number">${status.count + pagedData.size * pagedData.number}</span> --%>
									<a title="<spring:message code="faoInstitute.link-species-data" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}|${stat[0]}" argumentSeparator="|" />" href="<c:url value="/wiews/${faoInstitute.code}/t/${stat[0]}" />">
										<span dir="ltr" class="sci-name"><c:out value="${stat[0]}" /></span>
									</a>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 ${status.count % 2 == 0 ? 'even' : 'odd'}">
									<span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="chart chart-pie">
							<div id="chartStatsByGenus" style="height: 192px;"></div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<c:if test="${statisticsTaxonomy ne null and statisticsTaxonomy.numberOfElements gt 0}">
			<div class="collect-info col-lg-6 col-md-12 col-sm-12 col-xs-12 stat-by-species">
				<h4 class="row section-heading"><span><spring:message code="faoInstitute.stat-by-species" arguments="${statisticsTaxonomy.numberOfElements}" /></span></h4>
				<div class="section-inner-content clearfix ${cssClass}">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 statistics">
						<c:forEach items="${statisticsTaxonomy.content}" var="stat" varStatus="status">
							<div class="row">
								<div class="col-lg-8 col-md-6 col-sm-6 col-xs-6 ${status.count % 2 == 0 ? 'even' : 'odd'}">
									<%-- <span class="index-number">${status.count + pagedData.size * pagedData.number}</span> --%>
									<a title="<spring:message code="faoInstitute.link-species-data" arguments="${faoInstitute.fullName}|${faoInstitute.acronym}|${stat[0].genus} ${stat[0].species}" argumentSeparator="|" />" href="<c:url value="/wiews/${faoInstitute.code}/t/${stat[0].genus}/${stat[0].species}" />">
										<span dir="ltr" class="sci-name">
											<c:out value="${stat[0].taxonName}" />
										</span>
									</a>
								</div>
								<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
									<span class="stats-number"><fmt:formatNumber value="${stat[1]}" /></span>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="chart chart-pie">
							<div id="chartStatsBySpecies" style="height: 192px"></div>
						</div>
					</div>
				</div>
			</div>
		</c:if>

		<!-- See also end -->

	<c:if test="${statisticsPDCI.count gt 0 and statisticsPDCI.count eq countByInstitute}">
		<div class="collect-info col-lg-6 col-md-12 col-sm-12 col-xs-12 pdci">
			<h4 class="row section-heading"><span><spring:message code="accession.pdci" /></span></h4>
			<div class="section-inner-content clearfix ${cssClass}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="chart chart-histogram">
						<div id="chartPDCI" style="height: 200px"></div>
					</div>
					<p><spring:message code="accession.pdci.stats-text" arguments="${statisticsPDCI.elStats}" /></p>
					<p><a href="<c:url value="/content/passport-data-completeness-index" />"><spring:message code="accession.pdci.about-link" /></a></p>
					<security:authorize access="isAuthenticated()">
						<form class="form-horizontal" method="post" action="<c:url value="/download/wiews/${faoInstitute.code}/download" />">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<button name="pdci" class="btn btn-default" type="submit">
								<spring:message code="filter.download-pdci" />
							</button>
						</form>
					</security:authorize>
					<%-- <c:forEach items="${statisticsPDCI.histogram}" var="item" varStatus="index">
    						<div><c:out value="${index.count}=${item}" /></div>
  					</c:forEach> --%>
				</div>
			</div>
		</div>
	</c:if>
</div>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#faoInstitute, 'ADMINISTRATION')">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<a href="<c:url value="/acl/${faoInstitute.getClass().name}/${faoInstitute.id}/permissions"><c:param name="back">/wiews/${faoInstitute.code}</c:param></c:url>" class="close">
				<spring:message code="edit-acl" />
			</a>
			<a href="<c:url value="/wiews/${faoInstitute.code}/edit" />" class="close">
				<spring:message code="edit" />
			</a>
		</div>
	</security:authorize>

	<c:if test="${blurp ne null}">
		<div class="collect-info col-lg-12 col-md-12 col-sm-12 col-xs-12 about">
			<h4 class="row section-heading"><span><spring:message code="heading.about" /></span></h4>
			<div class="section-inner-content clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 two-column-text">
					<div property="schema:Organization#description"><cms:blurb blurb="${blurp}" /></div>
				</div>
			</div>
		</div>
	</c:if>

	<div class="collect-info col-lg-12 col-md-12 col-sm-12 col-xs-12 see-also">
		<h4 class="row section-heading"><span><spring:message code="heading.see-also" /></span></h4>
			<div class="section-inner-content clearfix">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="see-also">
						<c:if test="${countByInstitute gt 0}">
							<li>
								<a href="<c:url value="/wiews/${faoInstitute.code}/data/map" />">
									<spring:message code="see-also.map" />
								</a>
							</li>
							<li>
								<a href="<c:url value="/wiews/${faoInstitute.code}/overview" />">
									<spring:message code="see-also.overview" />
								</a>
							</li>
						</c:if>
							<li>
								<a href="<c:url value="/geo/${faoInstitute.country.code3}" />">
									<spring:message code="see-also.country" arguments="${faoInstitute.country.getName(pageContext.response.locale)}" />
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

	<content tag="javascript">

		<script>
			function checkWidth() {

				var mapHeight = $('.map-wrapper').height();

				if ($(window).innerWidth() > 989) {
					$('#map').css('height',mapHeight + 'px');
				}
			}

			$(document).ready(function() {
				checkWidth(true);

				$(window).resize(function() {
					checkWidth(true);
				});
			});
		</script>

		<script type="text/javascript">
			jQuery(document).ready(function() {
				var map=GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
					zoom: 0,
					minZoom: 0,
					maxZoom: 4, /* WIEWS does not provide enough detail */
					<c:if test="${faoInstitute.latitude ne null}">
					center: new GenesysMaps.LatLng(${faoInstitute.latitude}, ${faoInstitute.longitude}), 
					markerTitle: "<spring:escapeBody javaScriptEscape="true">${faoInstitute.code}</spring:escapeBody>",
					</c:if>
					scrollWheelZoom: false,
					touchZoom: false,
					dragging: false,
					doubleClickZoom: false,
					boxZoom: false,
					zoomControl:false
				});
			});
		</script>
   <script type="text/javascript">
			<%@ include file="/WEB-INF/jsp/wiews/ga.jsp" %>
			_pageDim = { institute: '<c:out value="${faoInstitute.code}" />"' };
		</script> <script>
        jQuery(document).ready(function () {
            GenesysChart.chart("#chartStatsByCropName", "<c:url value="/wiews/${faoInstitute.code}/stat-cropName" />", null, null, function(cropName) { window.location=window.location.pathname + "/cropName/" + cropName; });
            GenesysChart.chart("#chartStatsByGenus", "<c:url value="/wiews/${faoInstitute.code}/stat-genus" />", null, null, function(genus) { window.location=window.location.pathname + "/t/" + genus; });
            GenesysChart.chart("#chartStatsBySpecies", "<c:url value="/wiews/${faoInstitute.code}/stat-species" />", null, function(taxonomy) { return taxonomy.taxonName; }, function(taxonomy) { window.location=window.location.pathname + "/t/" + taxonomy.genus + "/" + taxonomy.species; });
            /* ${statisticsPDCI.count} eq ${countByInstitute} */
<c:if test="${statisticsPDCI.count gt 0 and statisticsPDCI.count eq countByInstitute}">
            GenesysChart.histogram("#chartPDCI", <c:out value="${statisticsPDCI.histogramJson}" escapeXml="false" /> );
</c:if>
        });
    </script> </content>
</body>
</html>