<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
  <title><spring:message code="crop.page.edit.title" arguments="${crop.name}" argumentSeparator="|" /></title>
</head>
<body>
<h1>
  <c:out value="${crop.name}" />
  <small><c:out value="${crop.shortName}" /></small>
</h1>

<form role="form" class="form-horizontal" action="<c:url value="/c/${crop.shortName}/update" />" method="post">

	<c:if test="${crop.name eq null}">
	  <div class="form-group">
	    <label for="blurp-body" class="col-lg-12 control-label"><spring:message code="crop.name-en" /></label>
	    <div class="controls col-lg-12">
			<input name="cropName" class="form-control" value="<c:out value="${crop.name}" />" />
	    </div>
	  </div>
	</c:if>

  <div class="form-group">
    <label for="blurp-body" class="col-lg-12 control-label"><spring:message code="blurp.blurp-body" /></label>
    <div class="controls col-lg-12">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
    </div>
  </div>

  <div class="form-group">
    <label for="crop-summary" class="col-lg-12 control-label"><spring:message code="crop.summary" /></label>
    <div class="controls col-lg-12">
				<textarea id="crop-summary" name="summary" class="span9 required html-editor">
					<c:out value="${blurp.summary}" />
				</textarea>
    </div>
  </div>

  <input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" /> <a href="<c:url value="/c/${crop.shortName}" />" class="btn btn-default"> <spring:message code="cancel" />
</a>
  <!-- CSRF protection -->
  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>


	<form role="form" class="form-horizontal" action="<c:url value="/c/${crop.shortName}/update" />" method="post">
		<div class="form-group">
			<label class="col-lg-12 control-label">
				<spring:message code="crop.cropname-aliases" />
			</label>
			<div class="controls col-lg-12">
				<textarea name="otherNames" class="form-control"><c:forEach items="${crop.otherNames}" var="otherName"><c:out value="${otherName}" />
</c:forEach></textarea>
			</div>
		</div>
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
	</form>

	<form role="form" class="form-horizontal" action="<c:url value="/c/${crop.shortName}/update" />" method="post">
		<div class="form-group">
			<label class="col-lg-12 control-label">
				<spring:message code="crop.cropname-aliases" />
			</label>
			<div class="controls col-lg-12">
				<textarea name="cropRules" class="form-control"><c:out value="${cropRulesJson}" /></textarea>
			</div>
		</div>
		<!-- CSRF protection -->
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<button name="action" value="update-rules" class="btn btn-primary"><spring:message code="save"/></button>
		<button name="action" value="update-taxonomy" class="btn btn-default">Re-link Taxonomy2 records</button>
	</form>

	<content tag="javascript">
  <script type="text/javascript">
    <local:tinyMCE selector=".html-editor" />
  </script>
</content>

</body>
</html>