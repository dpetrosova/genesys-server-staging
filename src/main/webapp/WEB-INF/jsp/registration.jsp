<%@ include file="init.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>

<html>
<head>
<title><spring:message code="registration.page.title" /></title>
</head>
<body>
	<h1 class="green-bg">
		<spring:message code="registration.title" />
	</h1>

	<security:authorize access="hasRole('ADMINISTRATOR')">
		<a href="<c:url value="/content/registration/edit" />" class="close"> <spring:message code="edit" />
		</a>
	</security:authorize>

	<cms:blurb blurb="${blurp}" />

	<gui:alert type="danger" display="${not empty error}">
		<spring:message code="${error}" />
	</gui:alert>

	<gui:alert type="error" display="${param['exist'] ne null}">
		<spring:message code="registration.user-exists" />
	</gui:alert>

	<form:form method="POST" action="/registration" cssClass="form-horizontal validate" modelAttribute="user">
		<div class="form-group">
			<form:label for="email" cssClass="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label" path="email">
				<spring:message code="registration.email" />
			</form:label>
			<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
				<form:input id="email" name="email" cssClass="span3 required email form-control" path="email"/>
			</div>
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12">
				<spring:bind path="email">
					<c:if test="${status.error}">
						<gui:alert type="danger">
							<spring:message code="${status.errorMessage}" />
						</gui:alert>
					</c:if>
				</spring:bind>
			</div>
		</div>
		<div class="form-group">
			<form:label for="password" cssClass="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label" path="password">
				<spring:message code="registration.password" />
			</form:label>
			<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
				<form:password id="password" name="password" cssClass="span3 required form-control" path="password"/>
			</div>
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12">
				<spring:bind path="password">
					<c:if test="${status.error}">
						<gui:alert type="danger">
							<spring:message code="${status.errorMessage}" />
						</gui:alert>
					</c:if>
				</spring:bind>

				<gui:alert type="danger" display="${not empty passwordError}">
					<spring:message code="${passwordError}" />
				</gui:alert>
			</div>
		</div>
		<div class="form-group">
			<label for="confirm_password" class="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label">
				<spring:message code="registration.confirm-password" />
			</label>
			<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
				<input type="password" id="confirm_password" name="confirm_password" class="span3 required form-control" equalTo="#password" />
			</div>
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12">
				<gui:alert type="danger" display="${not empty passwordRepeatError}">
					<spring:message code="${passwordRepeatError}" />
				</gui:alert>
			</div>
		</div>
		<div class="form-group">
			<form:label for="name" cssClass="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label" path="name">
				<spring:message code="registration.full-name" />
			</form:label>
			<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
				<form:input id="name" name="name" cssClass="span3 required form-control" path="name"/>
			</div>
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12">
				<spring:bind path="name">
					<c:if test="${status.error}">
						<gui:alert type="danger">
							<spring:message code="${status.errorMessage}" />
						</gui:alert>
					</c:if>
				</spring:bind>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 col-md-3 col-sm-3 col-xs-12 control-label"><spring:message code="captcha.text" /></label>
			<div class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
				<local:captcha siteKey="${captchaSiteKey}" />
			</div>
			<div class="col-lg-offset-2 col-lg-4 col-md-offset-3 col-md-4 col-sm-offset-3 col-sm-4 col-xs-12">
				<gui:alert type="danger" display="${not empty captchaError}">
					<spring:message code="${captchaError}" />
				</gui:alert>
			</div>
		</div>
		<div class="form-group transparent">
			<div class="col-lg-offset-2 col-lg-10 col-md-offset-3 col-md-10 col-sm-offset-3 col-sm-10 col-xs-12">
				<form:button class="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-12">
					<spring:message code="registration.create-account" />
				</form:button>
				<a class="btn btn-default col-lg-2 col-md-2 col-sm-2 col-xs-12"
					href='<c:url value="/" />' id="registration">
					<spring:message code="cancel" />
				</a>
			</div>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form:form>
</body>
</html>