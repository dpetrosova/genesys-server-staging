<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
  <head>
    <title><spring:message code="admin.page.title" /></title>
  </head>
  <body>
    <%@ include file="/WEB-INF/jsp/admin/menu.jsp" %>
    
	    <h3>Queues</h3>
	    <div>Remove queue size: <c:out value="${removeQueueSize}" /></div>
	    <div>Update queue size: <c:out value="${updateQueueSize}" /></div>

		<h3>Accession data</h3>
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<input type="text" name="filter" placeholder="Genesys filter {}" value="{}" />
			<button type="submit" class="btn btn-default" value="accn" name="reindex">Reindex</button>
			<button type="submit" class="btn btn-default" value="accn" name="regenerate">Regenerate</button>
		</form>
		
		<h3>Content</h3>
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<select name="type">
				<option value="All" selected>All content entities</option>
				<c:forEach items="${reindexTypes}" var="type">
					<option value="${type.value}"><c:out value="${type.key}" /></option>
				</c:forEach>
			</select>
			<button type="submit" class="btn btn-default" value="content" name="reindex">Reindex</button>
			<button type="submit" class="btn btn-default" value="content" name="regenerate">Regenerate</button>
		</form>
		
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<button type="submit" class="btn btn-default" name="clear-queues">Clear ES update queues</button>
		</form>
		
		<h3>Index alias management</h3>
		<form method="post" action="<c:url value="/admin/elastic/action" />">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			Alias:<input type="text" name="aliasName" />
			Index:<input type="text" name="indexName" />
			<button type="submit" class="btn btn-primary" value="realias" name="action">Move alias</button>
			<button type="submit" class="btn btn-default" value="delete-alias" name="action">Delete alias</button>
			<button type="submit" class="btn btn-default" value="delete-index" name="action">Delete index</button>
		</form>

	<h3>Indexes and aliases</h3>
    <c:if test="${empty indexes}">
      <h5>No indexes found.</h5>
    </c:if>
    <c:forEach items="${indexes}" var="indexMap" varStatus="i">
      <div>Index #<c:out value="${i.count}" />: <c:out value="${indexMap.key}" /></div>
      <c:choose>
        <c:when test="${empty indexMap.value}">
          <h5>No aliases found.</h5>
        </c:when>
        <c:otherwise>
          <table>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Filter</th>
              <th>Index routing</th>
              <th>Search routing</th>
            </tr>
            <c:forEach items="${indexMap.value}" var="alias" varStatus="j">
              <tr>
                <td><c:out value="${j.count}" /></td>
                <td><c:out value="${alias.alias}" /></td>
                <td><c:out value="${alias.filter}" /></td>
                <td><c:out value="${alias.indexRouting}" /></td>
                <td><c:out value="${alias.searchRouting}" /></td>
              </tr>
            </c:forEach>
          </table>
        </c:otherwise>
      </c:choose>
      <br/>
    </c:forEach>
  </body>
</html>