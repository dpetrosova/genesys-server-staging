<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><c:out value="${imageGallery.title}" /></title>
</head>
<body>
	<div class="free-text">
		<c:out value="${imageGallery.description}" />
	</div>
	<a href="<c:url value="/admin/r/g" />" class="btn btn-default"><spring:message code="cancel" /></a>
	<a href="<c:url value="/admin/r/files${imageGallery.path}" />" class="btn btn-default"><spring:message code="navigate.back" /></a>

	<div class="row" id="imagegallery-thumbs">
		<c:forEach items="${imageGallery.images}" var="image">
		<div x-src="<c:out value="${image.uuid}${image.extension}" />" class="col-xs-6 col-sm-3 col-md-2 col-lg-2">
			<img style="width: 100%; margin-bottom: 15px; margin-top: 15px;" src="<c:url value="/repository/d${image.path}_thumb/${thumbnailFormat}_${image.uuid}.png" />" alt="${image.title}" />
		</div>
		</c:forEach>
	</div>

	<div class="imagegallery-image-frame">
		<img id="imagegallery-image" />
	</div>

	<content tag="javascript"> <script type="text/javascript">
		$(document).ready(function() {
			var imageViewer=$('#imagegallery-image')[0];
			var baseHref = '<c:url value="/repository/d${imageGallery.path}" />';
			var galleryImages = $('#imagegallery-thumbs div');
			if (galleryImages.length > 0) {
				imageViewer.src = baseHref + $(galleryImages[0]).attr('x-src');
			}

			$('#imagegallery-thumbs div').click(function(ev) {
				imageViewer.src = baseHref + $(this).attr('x-src');
			});
		});
	</script> </content>
</body>
</html>
