<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="project.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="project.page.list.title" />
	</h1>

	<div class="main-col-header clearfix">
		<div class="nav-header">
			<local:paginate page="${pagedData}" />
		</div>
	</div>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasRole('VETTEDUSER')">
		<a href="<c:url value="/project/add-project" />" class="close">
			<spring:message code="add" />
		</a>
	</security:authorize>

	<ul class="funny-list">
		<c:forEach items="${pagedData.content}" var="project" varStatus="status">
			<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<a class="show pull-left" href="<c:url value="/project/${project.code}" />">
					<b>
						<c:out value="${project.code}" />
					</b>
					<c:out value="${project.name}" />
				</a>
			</li>
		</c:forEach>
	</ul>

</body>
</html>
