<%@ page contentType="text/javascript;charset=UTF-8" pageEncoding="UTF-8" language="java" %>/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * jQuery $.ajax() required.
 */
GenesysPGR = {
	defaultOptions: {
		startAt: 1,
		maxRecords: 50,
		success: new Function(),
		error: new Function()
	},
	
	getUrl: function(apiCall) {
		return '${baseUrl}/webapi' + apiCall + '?client_id=${clientId}&client_secret=${clientSecret}';
	},
	
	listAccessions: function (filter, opts) {
		var o = $.extend({}, GenesysPGR.defaultOptions, opts); 
		var json={filter: JSON.stringify(filter), startAt: o.startAt, maxRecords: o.maxRecords };

		$.ajax(GenesysPGR.getUrl('/v0/acn/filter'), {
			dataType: 'json',
			type: 'POST',
			contentType: 'application/json; charset=utf-8',
			data: JSON.stringify(json),

			success: function (accessions) {
				o.success(accessions);
			},

			error: function (errorAsync) {
				o.error(errorAsync);
			}
		});
	}
}
