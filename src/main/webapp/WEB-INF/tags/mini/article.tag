<%@ tag description="Display article" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/cms" %>
<%@ attribute name="article" required="true" type="org.genesys2.server.model.impl.Article" %>

<div class="genesys-mini genesy-article">
	<div class="title">
		<a href="<c:url value="/${article.lang}/content/${article.slug}" />">
			<c:out value="${article.title}" />
		</a>
	</div>
	<div class="summary free-text">
		<c:out escapeXml="false" value="${article.summary}" />
	</div>
	<div class="language">
		<em><c:out value="${article.lang}" /></em>
	</div>
</div>
