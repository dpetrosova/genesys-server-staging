<%@ tag description="Display country data" pageEncoding="UTF-8" %>
<%@ tag body-content="empty" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="local" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/cms" %>
<%@ attribute name="country" required="true"
	type="org.genesys2.server.model.impl.Country" %>

<div class="genesys-mini genesys-country">
	<div class="code3">
		<a href="<c:url value="/geo/${country.code3}" />"><c:out
				value="${country.code3}" /></a>
	</div>
	<div class="title">
		<c:out value="${country.getName(pageContext.response.locale)}" />
	</div>
</div>
